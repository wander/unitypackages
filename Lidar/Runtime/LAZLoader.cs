using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static Wander.Projections;

namespace LAZNamespace
{
    public enum CellGfxDataType
    {
        Mesh,
        Buffers
    }

    public enum CellDivideMethod
    {
        Linear,
        Quadratic2,
        Quadratic4,
        Quadratic8,
        Quadratic16,
    }

    public class TraceLineQuery
    {
        public Vector3 start;
        public Vector3 dir;
        public float radius2;
        public int pointSkipStep;
        // Output
        public LAZVertex vertex;
        public bool hit;
        public float hitDistance;
        public bool done;
        public LAZRenderer renderer;
    }
    
    [Serializable]
    public class CachedGfxObjects
    {
        public CellGfxDataType CacheType;
        public string Filename;
        public List<Mesh> MeshObjects;
        public List<Vector4[]> BufferPositions;
        public List<Vector4[]> BufferColors;
        public List<int> LeafIndices;
        public ulong NumPoints;
        public float XMin, YMin, ZMin;
        public float XMax, YMax, ZMax;
        public double centreX, centreY, centreZ;
    }

    [ExecuteAlways()]
    public class LAZLoader : MonoBehaviour
    {
        [Header("Stats")]
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public string CurrentLoadingFile;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float PercentageCompleted;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public string nameFirstLoadedOctree;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public List<string> loadedOctrees;

        [Header("Tree setup")]
        [Tooltip("Changes the graphics data type of an octree cell. Changing this requires reloading all files.")]
        public CellGfxDataType cellGfxDataType = CellGfxDataType.Mesh;
        [Tooltip("This determines the depth of the tree. Depth = Log( numPointCount / numPointsPerCell ) / Log( 8 ).")]
        public int numPointsPerCell = 10000;
        [Tooltip("Number of LODs per cell. Careful to not increase too high so that memory is preserved.")]
        public int numLODSPerCell = 4;
        [Tooltip("Max tree depth. Only set high if loading a very dense point cloud.")]
        public int maxTreeDepth = 3;
        [Tooltip("Max memory a point cloud can ocupy, if exceeding, points are skipped linearly to fit to max budget.")]
        public float maxMemorySizeGB = 4;
        [Tooltip("If true, the created meshes are saved so subsequent loads do not need to unpack the raw lidar files.")]
        public bool cacheToScene = true;
        [Tooltip("How LOD levels are computed. If linear and number of LOD levels is 4, then each new LOD is 25% less points. If Quadratic, then each subsequent LOD is halved or a quater (Quadratic4) etc.")]
        public CellDivideMethod divideMethod = CellDivideMethod.Quadratic8;

        [Header("Projection")]
        [Tooltip("Camera to use for LOD calculations.")]
        public Camera cam;
        [Tooltip("Target output projection.")]
        public Projection targetProjection = Projection.WebMercator;
        [Tooltip("Leave blank to use targetProject, otherwise attempt to convert WKT to projection is made.")]
        public string targetProjectionCustomWKT;

        [HideInInspector]
        public LAZOctree RelativeOctree { get; set; }
        //[HideInInspector]
        public List<CachedGfxObjects> cachedMeshes;

        bool done;
        string appDataPath;
        Vector3 camPosCopy;
        Plane [] frustumPlanes;
        Dictionary<string, LAZOctree> lazFiles;
        Dictionary<LAZRenderer, LAZOctree> renderers;
        List<TraceLineQuery> traceLineQueries;
        Task octreeTask;
        Task setupRenderingTask;
        Task traceTasks;

        [Header("Debug")]
        public bool seeIfOctreeTaskIsExecuting;
        public bool seeIfRendererTaskIsExecuting;

        internal string AppDataPath => appDataPath;

        internal static void GetSingle( ref LAZLoader loader )
        {
            if (loader != null)
                return;
            var loaders = FindObjectsOfType<LAZLoader>();
            if ( loaders != null && loaders.Length > 0 )
            {
                loader = loaders[0];
                if ( loaders.Length > 1 )
                {
                    Debug.LogWarning( "Multiple LAZLoaders found in scene, selected the first." );
                }
            }
        }

        private void Awake()
        {
            if (cam == null)
            {
                cam = FindObjectOfType<Camera>();
            }
        }

        void Setup()
        {
            nameFirstLoadedOctree = "";
            PercentageCompleted = 0;
            CurrentLoadingFile = "";
            done = false;
            appDataPath = Application.dataPath;
            frustumPlanes = new Plane[6];
            traceLineQueries = new List<TraceLineQuery>();
            loadedOctrees = new List<string>();

            // Only create if not already created. They are created on behalf of LazRenderer where OnEnable registers renderers.
            if (lazFiles == null)
            {
                lazFiles = new Dictionary<string, LAZOctree>();
                if (!lazFiles.ContainsValue( RelativeOctree ))
                    RelativeOctree = null;
            }
            if (renderers == null)
            {
                renderers = new Dictionary<LAZRenderer, LAZOctree>();
            }
            if (cachedMeshes == null)
            {
                cachedMeshes = new List<CachedGfxObjects>();
            }
            
            // Start async tasks.
            octreeTask          = Task.Run( OctreeTask );
            setupRenderingTask  = Task.Run( SetupRendererTask );
            traceTasks          = Task.Run( TraceTasks );
        }

        void Cleanup()
        {
            lock (traceLineQueries)
            {
                done = true;
                Monitor.Pulse( traceLineQueries);
            }
            octreeTask?.Wait();
            setupRenderingTask?.Wait();
            traceTasks?.Wait();
            if ( lazFiles != null )
            {
                foreach( var kvp in lazFiles )
                {
                    var tree = kvp.Value;
                    tree.Traverse( cb =>
                    {
                        if(  cb.gfxData != null )
                        {
                            cb.gfxData.Release();
                        }
                    } );
                }
            }
            lazFiles = null;
            renderers = null;
            cachedMeshes = null;
        }

        void OnEnable() => Setup();
        void OnAfterReload()
        {
            Setup();
        }

        void OnDestroy() => Cleanup();
        void OnDisable() => Cleanup();
        void OnBeforeReload() =>Cleanup();

        internal void ReloadFiles()
        {
            Cleanup(); // Stops threads.
            Setup();

            // Do meta search in scene on all renderers and swap there enable status.
            var lazRenderers = FindObjectsOfType<LAZRenderer>();
            for (int i = 0;i< lazRenderers.Length;i++)
            {
                lazRenderers[i].enabled = !lazRenderers[i].enabled;
                lazRenderers[i].enabled = !lazRenderers[i].enabled;
            }
        }

        internal void ClearCache()
        {
            cachedMeshes.Clear();
        }


        public TraceLineQuery QueueTraceLine( Vector3 start, Vector3 dir, int skipVerticesStep, float radius = 0.1f, float distance = 100 )
        {
            TraceLineQuery tlq = new TraceLineQuery();
            tlq.done = false;
            tlq.start = start;
            tlq.dir = dir;
            tlq.radius2 = radius*radius;
            tlq.hitDistance = distance;
            tlq.pointSkipStep = skipVerticesStep;
            lock (traceLineQueries)
            {
                traceLineQueries.Add( tlq );
                Monitor.Pulse( traceLineQueries );
            }
            return tlq;
        }

        internal void Update()
        {
            if (cam == null)
            {
                if (UnityEngine.Random.value < 0.1f)
                {
                    Debug.LogError( "No camera set, nothing will be streamed in!! Assign a camera in LAZ Loader in 'Projection' section." );
                }
                return;
            }

            // Copy frustum planes on main thread
            lock (frustumPlanes)
            {
                GeometryUtility.CalculateFrustumPlanes( cam, frustumPlanes );
                camPosCopy = cam.transform.position;
            }

            // Create gfx data which unity prevents to be created from another thread.
            bool lockTaken = false;
            Monitor.TryEnter( lazFiles, ref lockTaken );
            try
            {
                if (lockTaken)
                {
#if UNITY_EDITOR
                    loadedOctrees.Clear();
#endif

                    foreach (var kvp in lazFiles)
                    {
                        var tree = kvp.Value;
                        if (tree != null)
                        {
                            switch(tree.loadStage)
                            {
                                case LAZOctree.LoadStage.CreateGfxDataStage1MT:
                                    CreateGfxDataStage1MT( tree );
                                    break;

                                case LAZOctree.LoadStage.CreateGfxDataStage2MT:
                                    CreateGfxDataStage2MT( tree );
                                    break;

                                case LAZOctree.LoadStage.CreateGfxDataStage3MT:
                                    if (!tree.IsLoadedFromCache)
                                    {
                                        tree.CreateGfxDataStage3MT();
                                        if (cacheToScene)
                                        {
                                            BakeToScene( tree );
                                        }
                                    }
                                    break;
                            }
                        }

#if UNITY_EDITOR
                        if (RelativeOctree != null)
                            nameFirstLoadedOctree = RelativeOctree.Filename;
                        if ( kvp.Value.IsReady )
                            loadedOctrees.Add( kvp.Value.Filename );
#endif  
                    }
                }
            }
            finally
            {
                if (lockTaken) Monitor.Exit( lazFiles );
            }

            foreach (var kvp in renderers)
            {
                kvp.Key?.RenderOctree();
            }
        }

        private void BakeToScene( LAZOctree tree )
        {
            // Bake meshes to scene
            tree.GetAllGfxDatas( out List<LAZOctreeCellGfxData> gfxObjects, out List<int> leafIndices );
            CachedGfxObjects cml = new CachedGfxObjects();

            cml.CacheType = cellGfxDataType;
            if (cellGfxDataType == CellGfxDataType.Mesh)
            {
                cml.MeshObjects = gfxObjects.Select( gfx => (gfx as LAZOctreeCellMesh).Mesh ).ToList();
            }
            else
            {
                Debug.Assert( cellGfxDataType == CellGfxDataType.Buffers );
                gfxObjects.ForEach( gfx =>
                {
                    LAZOctreeCellBuffers buffers = ( gfx as LAZOctreeCellBuffers);
                    Vector4[] positions = new Vector4[buffers.PositionBuffer.count];
                    Vector4[] colors = new Vector4[buffers.ColorBuffer.count];
                    buffers.PositionBuffer.GetData( positions );
                    buffers.ColorBuffer.GetData( colors );
                    cml.BufferPositions = new List<Vector4[]>( buffers.PositionBuffer.count );
                    cml.BufferColors = new List<Vector4[]>( buffers.ColorBuffer.count );
                    cml.BufferPositions.Add( positions );
                    cml.BufferColors.Add( colors );
                } );

            }
            cml.Filename    = tree.Filename;
            cml.LeafIndices = leafIndices; // Leaf indices are necessary, because when rebuilding from cache, after prebuild, there is no way to know which leafs can be deleted as no vertices are put in yet. So Leaf indices, indicate which leafs must stay after prebuild.
            cml.XMin = tree.Min.x;
            cml.YMin = tree.Min.y;
            cml.ZMin = tree.Min.z;
            cml.XMax = tree.Max.x;
            cml.YMax = tree.Max.y;
            cml.ZMax = tree.Max.z;
            cml.centreX = tree.CentreX;
            cml.centreY = tree.CentreY;
            cml.centreZ = tree.CentreZ;
            cml.NumPoints = tree.NumPoints;
            cachedMeshes.RemoveAll( cml => cml.Filename == tree.Filename );
            cachedMeshes.Add( cml );
        }

        private void CreateGfxDataStage1MT( LAZOctree tree )
        {
            // Try from cache first.
            CachedGfxObjects cml = null;
            int cmlIdx = cachedMeshes.FindIndex( cml => cml.Filename == tree.Filename );
            if (cmlIdx >= 0)
            {
                cml = cachedMeshes[cmlIdx];
                Debug.Assert( tree.IsLoadedFromCache ); // Set in prebuilt step.
            }
            tree.CreateGfxDataStage1MT( cml );
        }

        private void CreateGfxDataStage2MT( LAZOctree tree )
        {
            tree.CreateGfxDataStage2MT();
        }

        internal LAZOctree RegisterRenderer( LAZRenderer renderer )
        {
            if (renderers == null)
                renderers = new Dictionary<LAZRenderer, LAZOctree>();

            var lazFile = AddLazFile( renderer.filename, renderer.sourceProjection, renderer.sourceProjectionWKT );
            if (lazFile != null)
            {
                lock (renderers)
                {
                    if (!renderers.ContainsKey( renderer ))
                    {
                        renderers.Add( renderer, lazFile );
                    }
                }
                return lazFile;
            }

            return null;
        }

        internal void UnregisterRenderer( LAZRenderer renderer )
        {
            if (renderers == null)
                return;
            lock (renderers)
            {
                renderers.Remove( renderer );
            }
        }

        LAZOctree AddLazFile( string filename, Projection sourceProjection, string sourceProjectionWKT )
        {
            if (lazFiles == null)
                lazFiles = new Dictionary<string, LAZOctree>();

            //     filename = Path.Combine( Application.dataPath, filename );

            if (string.IsNullOrEmpty( filename ))
                return null;

            // No need to put lock around this because this function is only accessed from main thead through LAZRenderer OnEnable().
            if (lazFiles.TryGetValue( filename, out LAZOctree file ))
                return file;

            LAZOctree laz = new LAZOctree( this, filename, sourceProjection, sourceProjectionWKT );
            if (RelativeOctree == null) // Assign first to a reference so it can be used to place other relative to this.
                RelativeOctree = laz;
            lock (lazFiles)
            {
                lazFiles.Add( filename, laz );
            }

            return laz;
        }

        // ------------------ Octree build tasks -------------------------------------------------------------------------------------------------

        void OctreeTask()
        {
            while (!done)
            {
                lock (lazFiles)
                {
                    foreach (var kvp in lazFiles)
                    {
                        LAZOctree tree = kvp.Value;
                        switch (tree.loadStage)
                        {
                            case LAZOctree.LoadStage.PrebuildAsync:
                            {
                                var error = tree.PreBuildAsync();
                                if (error != null)
                                {
                                    Debug.LogWarning( "Failed to preBuild: " + tree.Filename + ". Error: " + error );
                                }
                            }
                            break;

                            case LAZOctree.LoadStage.PostbuildAsync:
                            {
                                CurrentLoadingFile = tree.Filename;
                                PercentageCompleted = 0;
                                var error = tree.PostBuildAsync();
                                if (error != null)
                                {
                                    Debug.LogWarning( "Failed to postBuild: " + tree.Filename + ". Error: " + error );
                                }
                            }
                            break;

                            case LAZOctree.LoadStage.CreateGfxDataStage2Async:
                            {
                                tree.CreateGfxDataStage2Async();
                            }
                            break;
                        }
                    }
                }

                Thread.Sleep( 60 );
                if (seeIfOctreeTaskIsExecuting)
                {
                    Debug.Log( "Streamer executing" );
                }
            }
        }

        // ------------------ Set up render tasks -------------------------------------------------------------------------------------------------

        void SetupRendererTask()
        {
            while (!done)
            {
                // First undo interest for each lazfile
                lock (lazFiles)
                {
                    foreach (var kvp in lazFiles)
                    {
                        var tree = kvp.Value;
                        if (!tree.IsReady)
                            continue;
                        tree.RemoveAllPerCellInterestAsync();
                    }
                }

                // Compute and subscribe interest
                lock (renderers)
                {
                    foreach (var kvp in renderers)
                    {
                        var lazFile = kvp.Value;
                        if (lazFile != null && lazFile.IsReady)
                        {
                            var renderer = kvp.Key;
                            lazFile.ComputeAndSubscribePerCellInterestAsync( renderer, frustumPlanes, camPosCopy );
                        }
                    }
                }

                lock (lazFiles)
                {
                    foreach (var kvp in lazFiles)
                    {
                        var tree = kvp.Value;
                        if (tree.IsReady)
                        {
                            tree.AssignLODBasedRenderablesAsync();
                        }
                    }
                }

                Thread.Sleep( 60 ); 
                if (seeIfRendererTaskIsExecuting)
                {
                    Debug.Log( "Render executing" );
                }
            }
        }

        // ------------------ Trace Lines -------------------------------------------------------------------------------------------------
        void TraceLineAsync( TraceLineQuery q )
        {
            lock (renderers)
            {
                foreach (var kvpr in renderers)
                {
                    var renderer = kvpr.Key;
                    var lazFile  = kvpr.Value;
                    if (!lazFile.IsReady)
                        continue;
                    lazFile.TraceClosestPointAsync( renderer, q );
                }
            }
            q.hit = q.renderer != null;
            if (q.hit)
            {
                Debug.Assert( q.renderer );
                q.renderer.GetWorldTransform( out Matrix4x4 worldTransform );
                q.vertex.pos = worldTransform.MultiplyPoint( q.vertex.pos );
            }
            q.done = true;
        }

        void TraceTasks()
        {
            while (!done)
            {
                TraceLineQuery tlq = null;
                lock (traceLineQueries)
                {
                    if (traceLineQueries.Count > 0)
                    {
                        tlq = traceLineQueries[0];
                        traceLineQueries.RemoveAt( 0 );
                    }
                    if (!done && tlq == null)
                    {
                        Monitor.Wait( traceLineQueries );
                    }
                }
                if ( tlq != null)
                {
                    TraceLineAsync(tlq);  
                }
            }
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( LAZLoader ) )]
    [InitializeOnLoad]
    public class LAZStreamerEditor : Editor
    {
        static LAZStreamerEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            LAZLoader streamer = (LAZLoader)target;

            if (streamer.cellGfxDataType == CellGfxDataType.Buffers && streamer.maxTreeDepth > 1)
            {
                EditorGUILayout.HelpBox( "Max tree depth > 1 has no use (there is no lodding in 'Buffers' mode), it dramatically reduces performance. Set to 1 instead.", MessageType.Error, true );
                if (GUILayout.Button( "Fix and reload files" ))
                {
                    streamer.maxTreeDepth = 1;
                    streamer.ReloadFiles();
                }
            }

            if (streamer.cellGfxDataType == CellGfxDataType.Mesh && streamer.maxTreeDepth < 2)
            {
                EditorGUILayout.HelpBox( "Max tree depth < 2 causes no LODing, this makes rendering usually much slower.", MessageType.Error, true );
                if (GUILayout.Button( "Fix and reload files" ))
                {
                    streamer.maxTreeDepth = 4;
                    streamer.ReloadFiles();
                }
            }

            EditorGUILayout.HelpBox( "If the 'Tree setup' section is changed, cached meshes may not load back correctly, because the tree structure is not cached to disk. Always clear cache if altering the 'Tree setup'. This is not needed if 'Cache Meshes To Scene' is unticked.", MessageType.Warning, true );


            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Reload files" ))
                {
                    streamer.ReloadFiles();
                }
                if (GUILayout.Button( "Clear cache" ))
                {
                    streamer.ClearCache();
                }
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();
        }
    }
#endif
}