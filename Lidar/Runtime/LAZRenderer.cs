using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.VFX;
using Wander;

namespace LAZNamespace
{
    public enum ViewType
    {
        VertexColor,
        Elevation,
        Classification,
        Intensity
    }

    public enum PlaceType
    {
        RelativeToOneSpecific,
        BringToCentre
    }

    [ExecuteAlways()]
    public class LAZRenderer : MonoBehaviour
    {
        [Tooltip("The loader used for loading LIDAR data.")]
        public LAZLoader loader;

        [Header("Stats")]
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public ulong NumPoints;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public ulong NumUsedPoints;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public bool LoadedFromCache;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float PercentageLoaded;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float SizeGb;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float CompressRatio;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public int NumCells;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public int NumIntermediateCells;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public int NumLeafs;
#if UNITY_EDITOR        
        [ReadOnly()]
#endif
        [Tooltip("Filename to file. Must be in StreamingAssets folder or subdirectory thereof.")]
        public string filename;

        [Header("Placing & Projection")]
        [Tooltip("Source CRS (Coordinate Reference System). In future, allow to read this from WKT from file.")]
        public Projections.Projection sourceProjection = Projections.Projection.WebMercator;
        [Tooltip("Enter WKT to use that as CRS, otherwise sourceProjection above is used.")]
        public string sourceProjectionWKT;
        [Tooltip("Either place asset in centre of world or relative to a specific other asset.")]
        public PlaceType placeType = PlaceType.RelativeToOneSpecific;

        [Header("VFX Visualisation")]
        [Tooltip("Max particles, put -1 to obtain max of data.")]
        public int maxNumParticles = -1;
        [Tooltip("Particle size when rendering using VFX. Otherwise does nothing.")]
        public float size = 0.02f;
        [Tooltip("Particle color scale when rendering using VFX. Otherwise does nothing.")]
        public float colorScale = 1.0f;
        [Tooltip("The Viewtype. Note each viewtype has its own associated material.")]
        public ViewType viewType = ViewType.VertexColor;
        [Tooltip( "The vfx used for when viewtype is color." )]
        public VisualEffectAsset vfxColor;
        [Tooltip("The vfx used for elevation.")]
        public VisualEffectAsset vfxElevation;
        [Tooltip( "The vfx used for when viewtype is intensity." )]
        public VisualEffectAsset vfxIntensity;
        [Tooltip( "The vfx used for when viewtype is classification." )]
        public VisualEffectAsset vfxClassification;

        [Header("Mesh Visualisation")]
        [Tooltip("The material used for rendering when viewtype is color.")]
        public Material colorMaterial;
        [Tooltip("The material used for rendering when viewtype is elevation.")]
        public Material elevationMaterial;
        [Tooltip("The material used for rendering when viewtype is intensity.")]
        public Material intensityMaterial;
        [Tooltip("The material used for rendering when viewtype is classification.")]
        public Material classificationMaterial;
        [Tooltip("LOD to swtich when beyond thid distance.")]
        [Range(0.1f, 1000)]
        public float LODDistance = 10;
        [Tooltip("LOD logaritmic fallof control.")]
        [Range(0.01f, 1)]
        public float LODLogaritmicFalloff = 0.45f;
        [Tooltip("LOD bias to add from calculated lod.")]
        [Range(-10, 10)]
        public int LODBias = 0;

        [Header("Elevation")]
        [Range(0, 1)]
        public float elevationMin = 0;
        [Range(0, 1)]
        public float elevationMax = 1;
        public Color elevationColorLow = Color.blue;
        public Color elevationColorLowMid = Color.green;
        public Color elevationColorMidHigh = Color.yellow;
        public Color elevationColorHighTop = Color.red;

        [Header("Intensity")]
        [Range(0.0f, 1.0f)]
        public float intensityMin = 0;
        [Range(0.0f, 1.0f)]
        public float intensityMax = 1;
        public Color intensityColorLow = Color.blue;
        public Color intensityColorLowMid = Color.green;
        public Color intensityColorMidHigh = Color.yellow;
        public Color intensityColorHighTop = Color.red;

        [Header("Classification")]
        public Color [] classificationColors = new []
        {
            new Color(0.98f, 0.50f, 0.45f),
            new Color(1, 0.57f, 0.65f),
            new Color(0.76f, 0.7f, 0.5f),
            new Color(0.59f, 0.44f, 0.09f),
            new Color(0.96f, 0.64f, 0.38f),
            new Color(0.31f, 0.49f, 0.16f),
            new Color(0.06f, 0.32f, 0.73f),
            new Color(0.0f, 0.40f, 0.95f),
            new Color(0.18f, 0.66f, 0.63f),
            new Color(0.8f, 0.63f, 0.21f),
            new Color(1f, 0.14f, 0.0f),
            new Color(1f, 0.57f, 0.69f),
            new Color(1f, 0.85f, 0.0f),
            new Color(0.4f, 0.7f, 0.4f),
            new Color(0.4f, 1f, 0.6f),
            new Color(0.18f, 0.55f, 0.34f),
            new Color(0.0f, 1f, 0.8f),
            new Color(0.38f, 0.13f, 0.53f),
            new Color(0.2f, 0.08f, 0.08f),
        };

        [Header("Debug")]
        public bool showOctree = false;
        public bool onlyLeafs = true;
        [Range(-1, 20)]
        public int forceLOD = -1; 

        public Color octreeColor = Color.blue;
        public Color [] leafColors = new [] { Color.red, Color.yellow, Color.blue, Color.green, Color.cyan, Color.magenta, Color.white, Color.black };
        
        List<Tuple<LAZOctreeCell, int>> renderList;
        List<VisualEffect> vfxList;
        internal LAZOctree lazFile;
        Matrix4x4 [] worldTransform;
        bool vfxListDirty;

#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public Texture2D classificationMap;


        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        LAZLoader GetLoader()
        {
            LAZLoader.GetSingle( ref loader );
            return loader;
        }

        private void Awake()
        {
            if (Application.isEditor)
            {
                GetLoader();

#if UNITY_EDITOR
                colorMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "db7cdc62c3ba4e14599e4034ab4d091d" ) );
                elevationMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "be827dbdbb6ea8b49a4888b15da4f481" ) );
                intensityMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "7fbd909c5325d9c48966563c665e2f2e" ) );
                classificationMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "68bc4c0ada39be44aa98db8162ae8c43" ) );
                vfxElevation = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "c040bdee24cc9b54da9f9fb1f0a8900a" ) );
                vfxClassification = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "1c287fcc5b4d614468e11bb93e21734d" ) );
                vfxColor = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "0684482ab8d36f648924ace36ff37ed6" ) );
                vfxIntensity = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "f1912066fcc94894cad4f279e77de4d2" ) );
#endif
            }
        }

        private void OnEnable()
        {
            worldTransform = new Matrix4x4[1];
            worldTransform[0] = transform.localToWorldMatrix;
            renderList = new List<Tuple<LAZOctreeCell, int>>();
            if ((!string.IsNullOrEmpty( filename )) && (GetLoader() != null))
            {
                lazFile = loader.RegisterRenderer( this );
            }
        }

        private void OnDisable()
        {
            GetLoader()?.UnregisterRenderer( this );
        }

        internal void MakeRelativeToThis()
        {
            if ( lazFile != null )
            {
                GetLoader().RelativeOctree = lazFile;
            }
            else
            {
                Debug.LogWarning( "Cannot make relative if has not registered a LAS file." );
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal List<Tuple<LAZOctreeCell, int>> LockRenderList()
        {
            Monitor.Enter(renderList);
            return renderList;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void UnlockRenderList()
        {
            Monitor.Exit( renderList );
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        void UpdateWorldTransform()
        {
            if (placeType == PlaceType.RelativeToOneSpecific)
            {
                Debug.Assert( loader != null );
                LAZOctree referenceTree = (placeType == PlaceType.BringToCentre) ? lazFile : loader.RelativeOctree;
                if (referenceTree == null)
                {
                    // Could be that the first started loading asset takes longer than 2nd or later started, so this might be a null ref for a short time.
                    // In that case, dont apply the offset yet.
                    worldTransform[0] = transform.localToWorldMatrix;
                }
                else
                {
                    // TODO optimize with caching
                    double dx = referenceTree.CentreX - lazFile.CentreX;
                    double dy = referenceTree.CentreY - lazFile.CentreY;
                    double dz = referenceTree.CentreZ - lazFile.CentreZ;
                    var translation = Matrix4x4.Translate( -new Vector3((float)dx, (float)dy, (float)dz) ); // Since all assets are in local space to preserve precision. Now, back compensate the offset of the specific.
                    worldTransform[0] = transform.localToWorldMatrix * translation;
                }
            }
            else
            {
                worldTransform[0] = transform.localToWorldMatrix;
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void GetWorldTransform( out Matrix4x4 mat )
        {
            mat = worldTransform[0];
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        int GetLod(Mesh m, int lodFromProvider)
        {
            int chosenLod;
            if (forceLOD >= 0)
            {
                chosenLod = Mathf.Clamp( forceLOD, 0, m.subMeshCount-1 );
            }
            else
            {
                chosenLod = Mathf.Clamp( lodFromProvider+LODBias, 0, m.subMeshCount-1 );
            }
            return chosenLod;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private void CreateColorMap()
        {
            if (classificationMap == null || classificationMap.width < 256)
            {
                classificationMap = new Texture2D( 256, 1 );
                Color [] colors = new Color[256];
                for (int i = 0;i < Mathf.Min( classificationColors.Length, 256 );i++)
                {
                    colors[i] = classificationColors[i];
                }
                classificationMap.SetPixels( colors );
                classificationMap.Apply( false, true );
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private Material GetAndUpdateRenderMaterial()
        {
            // TODO optimize this, parameters only need to be set once when not running PIE.
            var matToUse = colorMaterial;
            switch (viewType)
            {
                case ViewType.Elevation:
                {
                    matToUse = elevationMaterial;
                    matToUse?.SetVector( "_MinMaxPoint", new Vector2( lazFile.Min.y, lazFile.Max.y ) );
                    matToUse?.SetColor( "_LowColor", elevationColorLow );
                    matToUse?.SetColor( "_LowMidColor", elevationColorLowMid );
                    matToUse?.SetColor( "_MidHighColor", elevationColorMidHigh );
                    matToUse?.SetColor( "_HighColor", elevationColorHighTop );
                    matToUse?.SetVector( "_MinMax", new Vector2( elevationMin, elevationMax ) );
                }
                break;

                case ViewType.Intensity:
                {
                    matToUse = intensityMaterial;
                    matToUse?.SetColor( "_LowColor", intensityColorLow );
                    matToUse?.SetColor( "_LowMidColor", intensityColorLowMid );
                    matToUse?.SetColor( "_MidHighColor", intensityColorMidHigh );
                    matToUse?.SetColor( "_HighColor", intensityColorHighTop );
                    matToUse?.SetVector( "_MinMax", new Vector2( intensityMin, intensityMax) );
                }
                break;

                case ViewType.Classification:
                {
                    CreateColorMap();
                    matToUse = classificationMaterial;
                    matToUse?.SetTexture( "_ColorMap", classificationMap );
                }
                break;
            }

            return matToUse;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private VisualEffectAsset GetVisualEffect()
        {
            VisualEffectAsset asset = vfxElevation;
            switch (viewType)
            {
                case ViewType.Intensity:
                    asset = vfxIntensity;
                    break;
                case ViewType.VertexColor:
                    asset = vfxColor;
                    break;
                case ViewType.Classification:
                    asset = vfxClassification;
                    break;
            }
            return asset;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        void UpdateVisualEffectParams(VisualEffect vfx, LAZOctreeCellBuffers cellBuffers)
        {
            Debug.Assert( vfx != null );

            uint numParticlesToShow = maxNumParticles==-1 ? (uint)lazFile.NumLoadedPoints : (uint)maxNumParticles;
            vfx.SetUInt( "count", numParticlesToShow );
            vfx.SetFloat( "size", size );
            vfx.SetGraphicsBuffer( "positionBuffer", cellBuffers.PositionBuffer );
            vfx.SetMatrix4x4( "transform", worldTransform[0] );
            vfx.SetFloat( "colorScale", colorScale );

            switch (viewType)
            {
                case ViewType.Elevation:
                {
                    vfx.SetVector2( "minMaxPoint", new Vector2( lazFile.Min.y, lazFile.Max.y ) );
                    vfx.SetVector2( "minMax", new Vector2( elevationMin, elevationMax ) );
                    vfx.SetVector4( "color1", elevationColorLow );
                    vfx.SetVector4( "color2", elevationColorLowMid );
                    vfx.SetVector4( "color3", elevationColorMidHigh );
                    vfx.SetVector4( "color4", elevationColorHighTop );
                }
                break;

                case ViewType.Intensity:
                    vfx.SetGraphicsBuffer( "colorBuffer", cellBuffers.ColorBuffer );
                    vfx.SetVector2( "minMax", new Vector2( intensityMin, intensityMax ) );
                    vfx.SetVector4( "color1", intensityColorLow );
                    vfx.SetVector4( "color2", intensityColorLowMid );
                    vfx.SetVector4( "color3", intensityColorMidHigh );
                    vfx.SetVector4( "color4", intensityColorHighTop );
                    break;

                case ViewType.VertexColor:
                    vfx.SetGraphicsBuffer( "colorBuffer", cellBuffers.ColorBuffer );
                    break;

                case ViewType.Classification:
                    CreateColorMap();
                    vfx.SetGraphicsBuffer( "colorBuffer", cellBuffers.ColorBuffer );
                    vfx.SetTexture( "classificationMap", classificationMap );
                    break;
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void RenderMesh()
        {
            Material matToUse = GetAndUpdateRenderMaterial();

            if (matToUse == null)
            {
                Debug.LogWarning( "Material is null. Cannot render without material." );
                return;
            }

            lock (renderList)
            {
                int count = renderList.Count;
                for (int i = 0;i < count;i++)
                {
                    LAZOctreeCellMesh cellMeshdata = renderList[i].Item1.gfxData as LAZOctreeCellMesh;
                    if (cellMeshdata == null)
                        break; // is null when changing mode from buffers to mesh while no files not yet reloaded
                    var mesh = cellMeshdata.Mesh;
                    int lod  = GetLod( mesh, renderList[i].Item2 );
                    Graphics.DrawMesh( mesh, worldTransform[0], matToUse, 0, null, lod, null, true );
                }
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void RenderBuffers()
        {
            Debug.Assert( loader.cellGfxDataType == CellGfxDataType.Buffers );

            var vfxAsset = GetVisualEffect();
            if (vfxAsset == null)
            {
                Debug.LogWarning( "Visual effect is null. Cannot render buffers without." );
                return;
            }

            lock (renderList)
            {
                int count = renderList.Count;

                // Lazy initialise the Vfx game objects.
                if (vfxList == null || vfxList.Count != count)
                {
                    // Delete former
                    var vfxs = gameObject.GetComponentsInChildren<VisualEffect>();
                    foreach (var fvx in vfxs)
                    {
                        fvx.gameObject.Destroy();
                    }
                    // Set up new
                    vfxList = new List<VisualEffect>( count );
                    for (int i = 0;i < count;i++)
                    {
                        GameObject goVfx = new GameObject("Vfx octree cell");
                        var vfx = goVfx.AddComponent<VisualEffect>();
                        vfx.visualEffectAsset = vfxAsset;
                        vfxList.Add( vfx );
                        goVfx.transform.SetParent( gameObject.transform );
                    }
                    // mark dirty 
                    vfxListDirty = true;
                }

#if UNITY_EDITOR
                // Only in editor, we can adjust this.
                for (int i = 0;i < count;i++)
                {
                    vfxList[i].visualEffectAsset = vfxAsset;
                }
                vfxListDirty = true;
#endif
                if (vfxListDirty)
                {
                    for (int i = 0;i < count;i++)
                    { 
                        LAZOctreeCellBuffers cellBuffers = renderList[i].Item1.gfxData as LAZOctreeCellBuffers;
                        UpdateVisualEffectParams( vfxList[i], cellBuffers );
                    }
                }
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void RenderOctree()
        {
            if (renderList == null)
                return;

            UpdateWorldTransform();

            if (loader.cellGfxDataType == CellGfxDataType.Mesh)
            {
                RenderMesh();
            }
            else
            {
                RenderBuffers();
            }
        }

        private void OnDrawGizmos()
        {
            if (lazFile == null)
                return;

            if (showOctree && !onlyLeafs)
            {
                Gizmos.color = octreeColor;
          //      UpdateWorldTransform();
                lazFile.Traverse( ( cell ) =>
                {
                    Vector3 centre = worldTransform[0].MultiplyPoint( cell.Centre );
                    Gizmos.DrawWireCube( centre, cell.HalfSize*2 );
                } );
            }
            else if (onlyLeafs)
            {
                if (loader.cellGfxDataType == CellGfxDataType.Mesh)
                {
                    lock (renderList)
                    {
                        int count = this.renderList.Count;
                        var renderList = this.renderList;
                        for (int i = 0;i < count;i++)
                        {
                            LAZOctreeCell cell = renderList[i].Item1;
                            var meshData = cell.gfxData as LAZOctreeCellMesh;
                            if (meshData == null) // when swapping mode mesh/buffers, this can happen
                                return;
                            int lod = GetLod( meshData.Mesh, renderList[i].Item2 );
                            var c = lod < 0 ? leafColors[0] : (lod >= leafColors.Length ? leafColors[leafColors.Length-1] : leafColors[lod]);
                            Vector3 centre = worldTransform[0].MultiplyPoint( cell.Centre );
                            Gizmos.color   = c;
                            Gizmos.DrawWireCube( centre, Vector3.Scale( cell.HalfSize*2, transform.localScale ) );
                        }
                    }
                }
                else
                {
                    // TODO impl buffers 
                }
            }
        }

//#if UNITY_EDITOR
//        [DrawGizmo( GizmoType.NotInSelectionHierarchy | GizmoType.Pickable | GizmoType.Selected )]
//        static void ExampleGizmo( LAZRenderer component, GizmoType type )
//        {
//            if (component == null || component.lazFile == null || component.worldTransform == null || component.worldTransform.Length==0 )
//                return;
//            bool selected = (type & GizmoType.Selected) > 0;
//            Color c = Color.cyan;
//            {
//                if (!selected)
//                    c.a = 0.25f;
//                Gizmos.color = c;
//                component.UpdateWorldTransform();
//                var centre = component.lazFile.Centre;
//                var min = component.lazFile.Min;
//                var max = component.lazFile.Max;
//                centre  = component.worldTransform[0].MultiplyPoint( centre );
//                Gizmos.DrawWireCube( centre, (max-min) );
//            }
//        }
//#endif
    }


#if UNITY_EDITOR
    [CustomEditor( typeof( LAZRenderer ) ), CanEditMultipleObjects]
    [InitializeOnLoad]
    internal class LAZRendererEditor : Editor
    {
        static LAZRendererEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            LAZRenderer lazRenderer = (LAZRenderer)target;

            ulong bufferParticleCapacity = 10000000;
            if ( lazRenderer.loader != null && lazRenderer.lazFile != null &&
                 lazRenderer.loader.cellGfxDataType == CellGfxDataType.Buffers &&
                 (lazRenderer.lazFile.NumLoadedPoints > bufferParticleCapacity  && 
                 (lazRenderer.maxNumParticles == -1 || lazRenderer.maxNumParticles > (long)bufferParticleCapacity) ) )
            {
                EditorGUILayout.HelpBox( "Max capacity of buffer point count is 10.000.000, not all points are rendererd.", MessageType.Warning, true );
            }

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Select file" ))
                {
                    string s = EditorUtility.OpenFilePanel( lazRenderer.filename, "", "" );
                    if (!string.IsNullOrEmpty( s ))
                    {
                        int t = s.LastIndexOf("StreamingAssets");
                        if (t != -1)
                        {
                            lazRenderer.filename = s.Substring( t );
                  //          lazRenderer.filename = s;
                            Repaint();
                            lazRenderer.enabled = !lazRenderer.enabled;
                            lazRenderer.enabled = !lazRenderer.enabled;
                        }
                        else
                        {
                            Debug.LogWarning( "File must be StreamingAssets or in any of its subdirectories." );
                        }
                    }
                }
                if (GUILayout.Button("Reposition all relative to this, this one becomes centre"))
                {
                    lazRenderer.MakeRelativeToThis();
                }
            }
            GUILayout.EndHorizontal();
            DrawDefaultInspector();

            // 
            UpdateInspectorStats( lazRenderer );
        }

        void UpdateInspectorStats( LAZRenderer lazRenderer )
        {
            if (lazRenderer == null)
                return;

            if (lazRenderer.lazFile != null)
            {
                lazRenderer.NumPoints = lazRenderer.lazFile.NumPoints;
                lazRenderer.NumUsedPoints = lazRenderer.lazFile.NumLoadedPoints;
                lazRenderer.PercentageLoaded = lazRenderer.lazFile.PercentageDone;
                lazRenderer.SizeGb = lazRenderer.lazFile.sizeGB;
                lazRenderer.CompressRatio = lazRenderer.lazFile.compressRatio;
                lazRenderer.NumCells = lazRenderer.lazFile.numCells;
                lazRenderer.NumIntermediateCells = lazRenderer.lazFile.numIntermediateCells;
                lazRenderer.NumLeafs = lazRenderer.lazFile.numLeafs;
            }
        }

    }
#endif
}