﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wander;

namespace LAZNamespace
{
    internal class LAZOctreeCellBuffers : LAZOctreeCellGfxData
    {
        GraphicsBuffer gbPosition;
        GraphicsBuffer gbColor;

        internal GraphicsBuffer PositionBuffer => gbPosition;
        internal GraphicsBuffer ColorBuffer => gbColor;

        bool LAZOctreeCellGfxData.IsCreated()
        {
            bool bCreated = ( gbPosition != null && gbColor != null );
            return bCreated;
        }

        void LAZOctreeCellGfxData.CreateGfxDataStage1MT( LAZOctreeCell cell, CachedGfxObjects cml, ref int leafIterator, ref int meshIterator )
        {
            if (cml == null)
            {
                // The vertex count per cell is not yet know at this stage.
            }
            else
            {
                // meshIterator can become bigger when there are leafs after the last valid leaf (that is: a leaf with a mesh).
                if (meshIterator < cml.LeafIndices.Count && cml.LeafIndices[meshIterator] == leafIterator)
                {
                    if (cml.BufferColors == null || cml.BufferColors == null)
                        throw new System.IO.InvalidDataException( "Cache invalid." );

                    Vector4 [] positions = cml.BufferPositions[meshIterator];
                    Vector4 [] colors = cml.BufferColors[meshIterator];

                    if (positions == null || colors == null)
                        throw new System.IO.InvalidDataException( "Cache invalid." );

                    meshIterator++;

                    gbPosition = new GraphicsBuffer( GraphicsBuffer.Target.Structured, 0, 16 );
                    gbColor = new GraphicsBuffer( GraphicsBuffer.Target.Structured, 0, 16 );
                    gbPosition.SetData( positions );
                    gbColor.SetData( colors );

                    // TODO do this somehow async. Need this for collision.
                    if (cell.originalVertices == null)
                    {
                        cell.originalVertices  = new List<LAZVertex>();
                        for (int i = 0;i < positions.Length;i++)
                        {
                            LAZVertex v = new LAZVertex();
                            v.pos = positions[i];
                            cell.originalVertices.Add( v );
                        }
                    }
                }
                leafIterator++;
            }
        }
        void LAZOctreeCellGfxData.CreateGfxDataStage2MT( LAZOctree tree, LAZOctreeCell cell )
        {
            gbPosition = new GraphicsBuffer( GraphicsBuffer.Target.Structured, cell.originalVertices.Count, 16 );
            gbColor = new GraphicsBuffer( GraphicsBuffer.Target.Structured, cell.originalVertices.Count, 16 );
            const float oneOver655356 = 1.0f / 65536;
            gbPosition.SetData( cell.originalVertices.Select( v =>
            {
                return new Vector4( v.pos.x, v.pos.y, v.pos.z, v.classification<<8 );
            } ).ToList() );
            gbColor.SetData( cell.originalVertices.Select( v =>
            {
                return new Vector4( v.r*oneOver655356, v.g*oneOver655356, v.b*oneOver655356, v.intensity );
            } ).ToList() );
        }

        void LAZOctreeCellGfxData.CreateGfxDataStage2Async( LAZOctree tree, LAZOctreeCell cell )
        {

        }

        void LAZOctreeCellGfxData.CreateGfxDataStage3MT()
        {

        }

        void LAZOctreeCellGfxData.AssignLODBasedRenderables( LAZOctreeCell cell )
        {
            var renderers = cell.renderers;
            foreach (var kvp in renderers)
            {
                if (PositionBuffer != null) // Is null when loading from cache failed because of invalid indices/iterator.
                {
                    var renderer   = kvp.Item1;
                    int indexStrip = kvp.Item2;
                    var list = renderer.LockRenderList();
                    list.RemoveSwapBack2( ( tuple ) => tuple.Item1 == cell );
                    list.Add( new Tuple<LAZOctreeCell, int>( cell, indexStrip ) );
                    renderer.UnlockRenderList();
                }
            }
        }

        void LAZOctreeCellGfxData.Release()
        {
            if (gbPosition != null) gbPosition.Release();
            if (gbColor != null) gbColor.Release();
            gbPosition = null;
            gbColor = null;
            GC.SuppressFinalize( this );
        }
    }
}
