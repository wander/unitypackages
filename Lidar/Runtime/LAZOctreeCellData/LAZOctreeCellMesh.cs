﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using Wander;

namespace LAZNamespace
{
    public interface LAZOctreeCellGfxData
    {
        internal bool IsCreated();

        // NOTE: At this stage originalVertices ar not loaded yet. Only the tree is prebuilt.
        internal void CreateGfxDataStage1MT( LAZOctreeCell cell, CachedGfxObjects cml, ref int leafIterator, ref int meshIterator );
        // At this stage, the OriginalVertices are loaded as the tree is postbuilt (loads vertices) and cleans up tree.
        internal void CreateGfxDataStage2MT( LAZOctree tree, LAZOctreeCell cell );
        // At this stage, the OriginalVertices are loaded as the tree is postbuilt (loads vertices) and cleans up tree.
        internal void CreateGfxDataStage2Async( LAZOctree tree, LAZOctreeCell cell );
        internal void CreateGfxDataStage3MT();
        internal void AssignLODBasedRenderables( LAZOctreeCell cell );
        internal void Release();
    }

    internal class LAZOctreeCellMesh : LAZOctreeCellGfxData
    {
        Mesh mesh; // Each lod level has its own submesh. 
        Mesh.MeshDataArray meshDataArray;
        bool disposed = true;

        internal Mesh Mesh => mesh;

        bool LAZOctreeCellGfxData.IsCreated()
        {
            return mesh != null;
        }

        void LAZOctreeCellGfxData.CreateGfxDataStage1MT( LAZOctreeCell cell, CachedGfxObjects cml, ref int leafIterator, ref int meshIterator )
        {
            Debug.Assert( mesh == null );
            if (cml == null)
            {
                mesh = new Mesh();
                meshDataArray = Mesh.AllocateWritableMeshData( 1 ); // Cannot be executed in other thread...
                disposed = false;
            }
            else
            {
                // meshIterator can become bigger when there are leafs after the last valid leaf (that is: a leaf with a mesh).
                if (meshIterator < cml.LeafIndices.Count && cml.LeafIndices[meshIterator] == leafIterator)
                {
                    mesh = cml.MeshObjects[meshIterator];
                    if (mesh == null)
                        throw new System.IO.InvalidDataException( "Cache invalid." );
                    meshIterator++; 

                    // TODO do this somehow async.
                    if (cell.originalVertices == null)
                    {
                        cell.originalVertices  = new List<LAZVertex>();
                        List<Vector3> vertices = new List<Vector3>();
                        mesh.GetVertices( vertices ); // We need these vertices from the mesh data to make collision detection possible when loaded from cache.
                        for (int i = 0;i < vertices.Count;i++)
                        {
                            LAZVertex v = new LAZVertex();
                            v.pos = vertices[i];
                            cell.originalVertices.Add( v );
                        }
                    }
                }
                leafIterator++;
            }
        }

        void LAZOctreeCellGfxData.CreateGfxDataStage2MT( LAZOctree tree, LAZOctreeCell cell )
        {

        }

        void LAZOctreeCellGfxData.CreateGfxDataStage2Async( LAZOctree tree, LAZOctreeCell cell )
        {
            // Setup vertices
            var vertexLayout = new[] // Layout must be multiple of 4
                {
                    new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3), /* cannot acces position.w in shader graph, so store intensity seperately */
                    new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.UNorm16, 4), /* color.a contains classification */
                    new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, 1), /* texcoord0.r contains intensity */
                };
            //       Mesh.MeshDataArray meshDataArray = Mesh.AllocateWritableMeshData(1); -> Can only be called on main thread
            Mesh.MeshData meshData = meshDataArray[0];
            meshData.SetVertexBufferParams( cell.originalVertices.Count, vertexLayout );
            NativeArray<LAZVertex> vertices = meshData.GetVertexData<LAZVertex>();
            vertices.CopyFrom( cell.originalVertices.ToArray() );

            // Make lambda method for getting numIndices
            int smallestPart = (cell.originalVertices.Count / tree.NumLodsPerCell);
            CellDivideMethod divideMethod = tree.DivideMethod;

            // Setup indices
            int totalIndices = 0;
            for (int i = 0;i <tree.NumLodsPerCell;i++)
            {
                int numIndices = getNumIndices(i, tree, cell, smallestPart, divideMethod );
                totalIndices += numIndices;
            }
            meshData.SetIndexBufferParams( totalIndices, IndexFormat.UInt32 );
            meshData.subMeshCount = tree.NumLodsPerCell;
            NativeArray<uint> indices = meshData.GetIndexData<uint>();
            int indexOffset = 0;
            for (int i = 0;i <tree.NumLodsPerCell;i++)
            {
                int numIndices = getNumIndices(i, tree, cell, smallestPart, divideMethod );
                // meshData.SetSubMesh( i, new SubMeshDescriptor( indexOffset, numIndices, MeshTopology.Points ) );
                // Build the indices
                float curIdx  = 0;
                int vertCount = cell.originalVertices.Count;
                float step    = cell.originalVertices.Count / (float)numIndices;
                for (int j = 0;j < numIndices;j++)
                {
                    uint index = (uint)Mathf.RoundToInt( curIdx );
                    if (index >= vertCount) index = (uint)(vertCount-1);
                    indices[indexOffset++] = index;
                    curIdx += step;
                }
                // var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontValidateIndices;
                //   Mesh.ApplyAndDisposeWritableMeshData( meshDataArray, mesh, flags );
            }
            Debug.Assert( indexOffset == totalIndices );
            indexOffset = 0;
            for (int i = 0;i <tree.NumLodsPerCell;i++)
            {
                int numIndices = getNumIndices(i, tree, cell, smallestPart, divideMethod );
                meshData.SetSubMesh( i, new SubMeshDescriptor( indexOffset, numIndices, MeshTopology.Points ) );
                indexOffset += numIndices;
            }
            Debug.Assert( indexOffset == totalIndices );
        }

        void LAZOctreeCellGfxData.CreateGfxDataStage3MT()
        {
            // Verified flags combination wiht RecalculateBounds afterwards. RecalculateBounds is neeeded, otherwise everything is clipped.
            // However, enabling it in the flags wont work...
            var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontValidateIndices;
            Mesh.ApplyAndDisposeWritableMeshData( meshDataArray, mesh, flags ); // Cannot be executed on other thread.
            mesh.RecalculateBounds();
            disposed = true;
            //meshDataArray = Mesh.AllocateWritableMeshData( 1 );
        }
        private static int getNumIndices( int i, LAZOctree tree, LAZOctreeCell cell, int smallestPart, CellDivideMethod divideMethod )
        {
            int numIndices = i==0 ? (cell.originalVertices.Count) : (smallestPart * (tree.NumLodsPerCell-i));
            if (i!= 0 && divideMethod != CellDivideMethod.Linear) // Linear is already calculated above.
            {
                float exp = 0;
                switch (divideMethod)
                {
                    case CellDivideMethod.Quadratic2:
                        exp = 0.5f;
                        break;
                    case CellDivideMethod.Quadratic4:
                        exp = 0.25f;
                        break;
                    case CellDivideMethod.Quadratic8:
                        exp = 0.125f;
                        break;
                    case CellDivideMethod.Quadratic16:
                        exp = 0.125f/2;
                        break;
                }
                numIndices =  Mathf.RoundToInt( (cell.originalVertices.Count) * Mathf.Pow( exp, i ) );
            }
            return Mathf.Max( 1, numIndices );
        }

        void LAZOctreeCellGfxData.AssignLODBasedRenderables(LAZOctreeCell cell)
        {
            var renderers = cell.renderers;
            foreach (var kvp in renderers)
            {
                if (mesh != null) // Is null when loading from cache failed because of invalid indices/iterator.
                {
                    var renderer   = kvp.Item1;
                    int indexStrip = kvp.Item2;
                    var list = renderer.LockRenderList();
                    list.RemoveSwapBack2( ( tuple ) => tuple.Item1 == cell );
                    list.Add( new Tuple<LAZOctreeCell, int>( cell, indexStrip ) );
                    renderer.UnlockRenderList();
                }
            }
        }

        void LAZOctreeCellGfxData.Release()
        {
            if (!disposed)
            {
                meshDataArray.Dispose();
                disposed = true;
            }
        }
    }
}