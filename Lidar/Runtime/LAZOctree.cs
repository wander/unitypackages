#define LAZ_USE_BUFFERS

using laszip.net;
using ProjNet.CoordinateSystems.Transformations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;
using Wander;
using static Wander.Projections;
using Debug = System.Diagnostics.Debug;


namespace LAZNamespace
{
    [System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Sequential )]
    public struct LAZVertex // Must be multiple of 4.
    {
        public Vector3 pos;
        public ushort r, g, b;
        public ushort classification; /* put classification in short, to make access in shader easier) -> UNorm16, x4 */
        public float intensity; // must maintain multiple of 4
        /*internal ushort intensity; */
    }


    internal class LAZOctreeCell
    {
        LAZOctree tree;
        Vector3 min;
        Vector3 max;
        int leafIdx;
        LAZOctreeCell [] cells;

        internal List<LAZVertex> originalVertices;
        internal LAZOctreeCellGfxData gfxData;
        internal List<Tuple<LAZRenderer, int>> renderers;

        public int LeafIndex => leafIdx;
        public Vector3 Min => min;
        public Vector3 Max => max;
        public Vector3 Centre => (max+min)*0.5f;
        public Vector3 HalfSize => (max-min)*0.5f;
        public bool IsLeaf => cells == null || cells.Length == 0;

        internal LAZOctreeCell( LAZOctree _tree )
        {
            tree = _tree;
        }

        // Prebuild using a predefined depth.
        // These coordinates are already projected into the target coordinates.
        // So all the min/max boxes should be in the target coordinates when the recursive function finishes.
        internal void PreBuild(Vector3 _min, Vector3 _max, int depth, ref int numLeafs)
        {
            min = _min;
            max = _max;
            if (depth > 1)
            {
                Vector3 [] mins;
                Vector3 [] maxs;
                GetMinMaxes( out mins, out maxs );
                cells = new LAZOctreeCell[8];
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i] = new LAZOctreeCell( tree );
                    cells[i].PreBuild( mins[i], maxs[i], depth-1, ref numLeafs );
                }
            }
            else
            {
                leafIdx = numLeafs;
                numLeafs++;
            }
        }

        // Because the structure is built with a predefined size, there might be many empty cells. Clean them.
        internal void CleanEmptyCells(LAZOctreeCell parent, int childIdx, ref int numDeletedCells )
        {
            if ( cells != null )
            {
                for( int i = 0; cells != null /*becomes null from cell setting parent.cell=null*/ && i < cells.Length; i++ )
                {
                    cells[i]?.CleanEmptyCells( this, i, ref numDeletedCells );
                }
            }
            else if ( (originalVertices == null && !tree.IsLoadedFromCache ) || (gfxData == null && tree.IsLoadedFromCache) ) /* When loaded from cache, GFX data is immediately created. */
            {
                if (parent != null)
                {
                    numDeletedCells++;
                    parent.cells[childIdx] = null;
                    // If all cells of parent have nulled, remove entire child tree.
                    bool allNull = true;
                    for (int i = 0;i < 8;i++)
                    {
                        if (parent.cells[i] != null)
                        {
                            allNull = false;
                            break;
                        }
                    }
                    if (allNull)
                    {
                        parent.cells = null;
                    }
                }
            }
        }

        internal LAZOctreeCell FindCell( ref Vector3 point )
        {
            if ( point.x < min.x || point.x > max.x || point.y < min.y || point.y > max.y || point.z < min.z || point.z > max.z )
            {
                return null;
            }
            if ( cells != null )
            {
                for ( int i = 0; i < cells.Length; i++ )
                {
                    var cell = cells[i].FindCell( ref point );
                    if (cell != null)
                        return cell;
                }
                return null;
            }
            else return this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void AddVertexIndex( ref LAZVertex vertex ) 
        {
            // By adding a vertex, this cell becomes a verified leaf automatically.
            Debug.Assert( cells == null );
            if (originalVertices == null)      originalVertices = new List<LAZVertex>();
            originalVertices.Add( vertex );
        }

        void GetMinMaxes( out Vector3[] minN, out Vector3[] maxN )
        {
            Vector3 hs = (max - min) * 0.5f;
            minN = new Vector3[8];
            maxN = new Vector3[8];
            // --------------------------------------
            minN[0] = min;
            maxN[0] = minN[0] + hs;
            minN[1] = min + new Vector3( hs.x, 0, 0 );
            maxN[1] = minN[1] + hs;
            minN[2] = min + new Vector3( hs.x, 0, hs.z );
            maxN[2] = minN[2] + hs;
            minN[3] = min + new Vector3( 0, 0, hs.z );
            maxN[3] = minN[3] + hs;
            // --------------------------------------
            minN[4] = min + new Vector3( 0, hs.y, 0 );
            maxN[4] = minN[4] + hs;
            minN[5] = min + new Vector3( hs.x, hs.y, 0 );
            maxN[5] = minN[5] + hs;
            minN[6] = min + new Vector3( hs.x, hs.y, hs.z );
            maxN[6] = minN[6] + hs;
            minN[7] = min + new Vector3( 0, hs.y, hs.z );
            maxN[7] = minN[7] + hs;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        bool IsSphereOnNegativePlaneSide( Plane p, Vector3 c, float r )
        {
            float d = Vector3.Dot( p.normal, c ) - p.distance;
            return d < r;
            //if (d > r)  return false;
            //if (-d < r) return false;
            //return true;
        }

        internal bool IsVisible( Plane[] frustumPlanes, ref Matrix4x4 worldTransform, out Vector3 centreWorld )
        {
            float r     = (max - min).magnitude;
            centreWorld = worldTransform.MultiplyPoint( Centre );
            for (int i = 0;i < 6;i++)
            {
                if (!IsSphereOnNegativePlaneSide( frustumPlanes[i], centreWorld, r ))
                    return false;
            }
            return true;
            //{ TODO (TestPlaneAABB cannot be executed on other thread.
            //    Bounds b = new Bounds();

            //    b.min = min + worldPosition;
            //    b.max = max + worldPosition;
            //    if (GeometryUtility.TestPlanesAABB( frustumPlanes, b ))
            //    {
            //        return true;
            //    }
            //    return false;
            //}
        }

        internal void ComputeAndSubscribePerCellInterestAsync(LAZRenderer renderer, Plane [] frustumPlanes, Vector3 camOrigin, ref Matrix4x4 worldTransform )
        {
            IsVisible( frustumPlanes, ref worldTransform, out Vector3 centreWorld );
                //return; // TODO

            if ( cells == null )
            {
                var lodIndex = Vector3.Distance( camOrigin, centreWorld ) / renderer.LODDistance;
                float normalizedDist = Mathf.Clamp01( lodIndex / renderer.loader.numLODSPerCell );
                normalizedDist = (float)Math.Pow( normalizedDist, 1/  renderer.LODLogaritmicFalloff );
                int lod =  Mathf.RoundToInt( normalizedDist * (tree.NumLodsPerCell-1) );
                //       renderers.RemoveSwapBack( ( tuple ) => tuple.Item1 == renderer );
                if (renderers == null)
                    renderers = new List<Tuple<LAZRenderer, int>>();
                renderers.Add( new Tuple<LAZRenderer, int>( renderer, lod ) );
            }
            else 
            {
                for (int i = 0;i < cells.Length; i++)
                {
                    cells[i]?.ComputeAndSubscribePerCellInterestAsync( renderer, frustumPlanes, camOrigin, ref worldTransform );
                }
            }
        }

        internal void RemoveAllPerCellInterestAsync()
        {
            if ( cells == null )
            {
                renderers?.Clear();
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i]?.RemoveAllPerCellInterestAsync();
                }
            }
        }

        internal void CreateGfxDataStage1MT( LAZLoader loader, CachedGfxObjects cml, ref int leafIterator, ref int meshIterator)
        {
            if (cells == null)
            {
                Debug.Assert( originalVertices==null );
                Debug.Assert( renderers == null );
                Debug.Assert( gfxData == null );
                switch ( loader.cellGfxDataType )
                {
                    case CellGfxDataType.Buffers:
                        gfxData = new LAZOctreeCellBuffers();
                        break;
                    case CellGfxDataType.Mesh:
                        gfxData = new LAZOctreeCellMesh();
                        break;
                }
                gfxData.CreateGfxDataStage1MT( this, cml, ref leafIterator, ref meshIterator );
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i]?.CreateGfxDataStage1MT( loader, cml, ref leafIterator, ref meshIterator );
                }
            }
        }

        internal void CreateGfxDataStage3MT()
        {
            if (cells == null)
            {
                gfxData.CreateGfxDataStage3MT();
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i]?.CreateGfxDataStage3MT();
                }
            }
        }

        internal void GetAllGfxDatas( ref List<LAZOctreeCellGfxData> cellGfxDatas, ref List<int> cellIndices )
        {
            if (cells == null)
            {
                Debug.Assert( gfxData != null );
                cellGfxDatas.Add( gfxData );
                cellIndices.Add( leafIdx );
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i]?.GetAllGfxDatas( ref cellGfxDatas, ref cellIndices );
                }
            }
        }

        internal void CreateGfxDataStage2MT( LAZOctree tree )
        {
            if (cells == null)
            {
                Debug.Assert( originalVertices!=null );
                gfxData.CreateGfxDataStage2MT( tree, this );
            }
            else
            {
                for (int i = 0;i<cells.Length;i++)
                {
                    cells[i]?.CreateGfxDataStage2MT( tree );
                }
            }
        }

        internal void CreateGfxDataStage2Async( LAZOctree tree )
        {
            if (cells == null)
            {
                Debug.Assert( originalVertices!=null );
                gfxData.CreateGfxDataStage2Async( tree, this );
            }
            else
            {
                for (int i = 0;i<cells.Length;i++)
                {
                    cells[i]?.CreateGfxDataStage2Async( tree );
                }
            }
        }

        internal void AssignLODBasedRenderables()
        {
            if ( cells == null )
            {
                if (renderers != null)
                {
                    gfxData.AssignLODBasedRenderables( this );
                }
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i]?.AssignLODBasedRenderables();
                }
            }
        }

        internal void TraceClosestPoint( Ray rayInvTransformWorld, TraceLineQuery q, LAZRenderer renderer )
        {
            Bounds b = new Bounds(Centre, HalfSize*2);
            if (b.IntersectRay( rayInvTransformWorld, out _ ))
            {
                if (cells == null)
                {
                    if (originalVertices == null) // Was null when loaded from cache or when not implemeted for the specific cell data type.
                        return;

                    Debug.Assert( tree.IsReady );
                    for ( int i = 0; i < originalVertices.Count; i += q.pointSkipStep )
                    {
                        Vector3 point = originalVertices[i].pos;
                        Vector3 pointToRayOrigin = point - rayInvTransformWorld.origin;
                        float newDist = Vector3.Dot(rayInvTransformWorld.direction, pointToRayOrigin);
                        if ( newDist >= 0 && newDist < q.hitDistance ) // otherwise point is behind origin
                        {
                            Vector3 dirToLine = Vector3.Cross( rayInvTransformWorld.direction, pointToRayOrigin );
                            float distSq = Vector3.Dot(dirToLine, dirToLine);
                            if ( distSq < q.radius2 )
                            {
                                q.hitDistance = newDist;
                                q.vertex = originalVertices[i];
                                q.renderer = renderer;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0;i < cells.Length;i++)
                    {
                        cells[i]?.TraceClosestPoint( rayInvTransformWorld, q, renderer );
                    }
                }
            }
        }

        internal void Verify() 
        {
            if (cells == null)
            {
                if ( !tree.IsLoadedFromCache )
                {
                    Debug.Assert( originalVertices != null );
                    Debug.Assert( gfxData != null );
                }
            }
            else
            {
                Debug.Assert( originalVertices == null );
                Debug.Assert( renderers == null );
                Debug.Assert( gfxData == null );
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i]?.Verify();
                }
            }
        }

        internal void Traverse( Action<LAZOctreeCell> cb )
        {
            cb( this );
            if (cells != null)
            {
                for (int i = 0;i < 8;i++)
                {
                    cells[i]?.Traverse( cb );
                }
            }
        }

        internal bool Contains( ref Vector3 point )
        {
            if (point.x < min.x || point.x > max.x || point.y < min.y || point.y > max.y || point.z < min.z || point.z > max.z)
            {
                return false;
            }
            return true;
        }
    }

    public class LAZOctree
    {
        internal enum LoadStage
        {
            PrebuildAsync,
            CreateGfxDataStage1MT, // When loaded from cache, after this stage, it is immediately ready.
            PostbuildAsync,
            CreateGfxDataStage2MT,
            CreateGfxDataStage2Async,
            CreateGfxDataStage3MT,
            Ready
        }

        internal LoadStage loadStage = LoadStage.PrebuildAsync;

        string filename;
        LAZLoader loader;
        LAZOctreeCell root;
        laszip_dll lazReader;
        double centreX, centreY, centreZ; // Places coordinates back in absolute space. Keep in doubles.
        Vector3 min, max; // min-max in local coordinates (after subtracting centre).
        ulong numberOfPoints;
        ulong numberOfLoadedPoints;

        // stats
        public int numLeafs;
        public int numCells;
        public int numIntermediateCells;
        public float sizeGB;
        public float compressRatio;

        public float PercentageDone { get; private set; }
        internal bool IsLoadedFromCache { get; set; }
        internal bool IsReady => (loadStage == LoadStage.Ready);
        internal string Filename => filename;
        internal double CentreX => centreX;
        internal double CentreY => centreY;
        internal double CentreZ => centreZ;
        internal Vector3 Min => min;
        internal Vector3 Max => max;
        internal ulong NumPoints => numberOfPoints;
        internal ulong NumLoadedPoints => numberOfLoadedPoints;
        internal int NumLodsPerCell => loader.numLODSPerCell;
        internal int NumPointsPerCell => loader.numPointsPerCell;
        internal CellDivideMethod DivideMethod => loader.divideMethod;
        internal laszip_dll LazReader => lazReader;
        internal ICoordinateTransformation crs;

        internal LAZOctree( LAZLoader loader, string _filename, Projection sourceProjection, string sourceProjectionWKT )
        {
            this.loader = loader;
            filename = _filename;
            bool src = sourceProjectionWKT.HasContent();
            bool tar = loader.targetProjectionCustomWKT.HasContent();
            if ( src && tar )
            {
                crs = CreateProjection( sourceProjectionWKT, loader.targetProjectionCustomWKT );
            }
            else if ( !src && tar )
            {
                crs = CreateProjection( sourceProjection, loader.targetProjectionCustomWKT );
            }
            else if ( src && !tar )
            {
                crs = CreateProjection( sourceProjectionWKT, loader.targetProjection );
            }
            else crs = CreateProjection( sourceProjection, loader.targetProjection );
            Debug.Assert( crs != null );
        }

        internal void Reset()
        {
            PercentageDone = 0;
            loadStage = LoadStage.PrebuildAsync;
            IsLoadedFromCache = false;
            if (lazReader != null)
            {
                lazReader.laszip_close_reader();
            }
        }

        internal bool AreSourceAndTargetCRSEqual()
        {
            // TODO more sophisticated
            return crs.SourceCS.Name == crs.TargetCS.Name;
        }

        internal string PreBuildAsync()
        {
            Debug.Assert( loader != null );

            if (lazReader != null)
                lazReader.laszip_close_reader(); 

            int cmlIdx = loader.cachedMeshes.FindIndex( cml => cml.Filename == filename );
            if (cmlIdx < 0)
            {
                lazReader = new laszip_dll();
                var compressed = true;
                var loadName = Path.Combine( loader.AppDataPath, filename );
                if (0 != lazReader.laszip_open_reader( loadName, ref compressed ))
                {
                    return lazReader.laszip_get_error();
                }
                numberOfPoints = lazReader.header.number_of_point_records;

                ( double x, double y, double z) maxTransformed = (lazReader.header.max_x, lazReader.header.max_y, lazReader.header.max_z);
                ( double x, double y, double z) minTransformed = (lazReader.header.min_x, lazReader.header.min_y, lazReader.header.min_z);
                if (!AreSourceAndTargetCRSEqual())
                {
                    maxTransformed = crs.MathTransform.Transform( lazReader.header.max_x, lazReader.header.max_y, lazReader.header.max_z );
                    minTransformed = crs.MathTransform.Transform( lazReader.header.min_x, lazReader.header.min_y, lazReader.header.min_z );
                }

                // Note swap y/z
                centreX = (maxTransformed.x+minTransformed.x)*0.5;
                centreY = (maxTransformed.z+minTransformed.z)*0.5;
                centreZ = (maxTransformed.y+minTransformed.y)*0.5;

                min = new Vector3(
                    (float)(minTransformed.x-centreX),
                    (float)(minTransformed.z-centreY/*centre Z already swapped with Y*/),
                    (float)(minTransformed.y-centreZ) );

                max = new Vector3(
                    (float)(maxTransformed.x-centreX),
                    (float)(maxTransformed.z-centreY/*centre Z already swapped with Y*/),
                    (float)(maxTransformed.y-centreZ) );

            }
            else
            {
                CachedGfxObjects cml = loader.cachedMeshes[cmlIdx];
                min = new Vector3( cml.XMin, cml.YMin, cml.ZMin );
                max = new Vector3( cml.XMax, cml.YMax, cml.ZMax );
                centreX = cml.centreX;
                centreY = cml.centreY;
                centreZ = cml.centreZ;
                numberOfPoints = (uint) cml.NumPoints;
                IsLoadedFromCache = true;
            }


            if ( !(Min.IsSane() && Max.IsSane() && CentreX.IsSane() && CentreY.IsSane() && CentreZ.IsSane()) ||
                  !Vector3.Distance(Min, Max).IsSane()  ||
                  (Vector3.Distance(Min, Max) == 0 ) )
            {
                // By returning here, the preBuilt=true is not set, as a result it will keep attempting to build this octree. // No invalid build state is created.
                // If coordinate calculation is correct, it will resumue building the remaining part.
                return "Invalid coordinates calculated, is the source and target projection correct?";
            }

            // Determine the tree depth.
            root = new LAZOctreeCell( this );
            int depth = Mathf.CeilToInt( Mathf.Log( numberOfPoints / (float)NumPointsPerCell ) / Mathf.Log( 8 ) );
            depth = Mathf.Clamp( depth, 0, loader.maxTreeDepth );

            // Prebuild octree structure because otherwise for every single cell we have to go through all points again.
            Stopwatch sw = new Stopwatch();
            sw.Restart();
            int numLeafs = 0;
            root.PreBuild( min, max, depth, ref numLeafs );
            sw.Stop();
       //     UnityEngine.Debug.Log( "Prebuild in: " + sw.ElapsedMilliseconds + "ms" );

            loadStage = LoadStage.CreateGfxDataStage1MT;
            return null; /* No Error */
        }

        internal string PostBuildAsync()
        {
            Stopwatch sw = new Stopwatch();
            int insertedPoints = 0;
            if (!IsLoadedFromCache)
            {
                // Get all or percentage of points.
                // coordinates coordArray = new coordinates();
                double [] coordArray = new double[3];
                float oneOver65535 = 1.0f / 65535;
                LAZVertex vt = new LAZVertex();
                sizeGB = (/*sizeof(LAZVertex)*/ (12+6+2+4) /*point & rgb & classification & intensity */ * (float)numberOfPoints) / (1073741824.0f /*to gb*/);
                compressRatio = sizeGB / loader.maxMemorySizeGB;
                compressRatio = Mathf.Max( 0, (compressRatio-1) );
                float compressAccum = 0;

                // Check if source and target crs are same, to avoid unecessary calcs
                bool areSourceAndTargetCRSEqual = AreSourceAndTargetCRSEqual();

                // For each point, find cell and add index.
                sw.Restart();
                LAZOctreeCell cell = null;
                float hundredOverNumPoints = 100.0f / numberOfPoints;
                for (uint i = 0;i < numberOfPoints;i++)
                {
                    int err = lazReader.laszip_read_point();
                    if (err != 0)
                    {
                        return lazReader.laszip_get_error();
                    }

                    if (compressAccum <= 1)
                    {
                        //lazReader.laszip_get_coordinates( ref coordArray );
                        lazReader.laszip_get_coordinates( coordArray );
                        numberOfLoadedPoints++;

                        //var transfomredResult = Transform( crs, (float)coordArray.x, (float)coordArray.y ); // Note: The .z holds the height.
                        if (!areSourceAndTargetCRSEqual)
                        {
                            crs.MathTransform.Transform( ref coordArray[0], ref coordArray[1], ref coordArray[2] );
                        }

                        Debug.Assert( coordArray[0].IsSane() && coordArray[1].IsSane() && coordArray[2].IsSane() );

                        vt.pos.x = (float)(coordArray[0]-centreX);
                        vt.pos.y = (float)(coordArray[2]-centreY); // Note Swap z/y, centre is already swapped.
                        vt.pos.z = (float)(coordArray[1]-centreZ);
                        vt.r = lazReader.point.rgb[0]; // colors stoed as non-normalized, shader will normalize
                        vt.g = lazReader.point.rgb[1];
                        vt.b = lazReader.point.rgb[2];
                        vt.classification = (ushort)(lazReader.point.classification<<8); // stored in color.a in shader as non-normalized value, shader will normalize
                        vt.intensity = 1-(LazReader.point.intensity*oneOver65535); // stored in uv0.r in shader as normalized, shader wont normalize

                        if (cell == null || !cell.Contains( ref vt.pos ))
                        {
                            cell = root.FindCell( ref vt.pos );
                        } 
                        if (cell != null) // It might still be null due to a rounding error where a point is on the edge of the enclosed bounding box.
                        {
                            cell.AddVertexIndex( ref vt );
                            insertedPoints++;
                        }
                        compressAccum += compressRatio;
                    }
                    else compressAccum -= 1;

                    // Update loading stats
                    PercentageDone = (i+1) * hundredOverNumPoints;
                    loader.PercentageCompleted = PercentageDone;
                }
                sw.Stop();
           //     UnityEngine.Debug.Log( "Reading points took: " + sw.ElapsedMilliseconds + "ms" );

                lazReader.laszip_close_reader();
            }
            else
            {
                PercentageDone = 100;
            }

            // Because the octree is prebuilt, we may end up with many empty cells.
            sw.Restart();
            int numLeafsDeleted = 0;
            while (true)
            {
                int curNumLeafsDeleted = 0;
                root.CleanEmptyCells( null, -1, ref curNumLeafsDeleted );
                if (curNumLeafsDeleted == 0)
                    break;
                numLeafsDeleted += curNumLeafsDeleted;
            }
            sw.Stop();
        //    UnityEngine.Debug.Log( "Delete obsolete leafs: " + numLeafsDeleted + " in:" + sw.ElapsedMilliseconds +"ms" );

            if (LazReader != null) // Can be null if was loaded from cache.
            {
                lazReader.laszip_close_reader();
                lazReader = null;
            }

#if UNITY_EDITOR
            // Verify that if a cell is leaf, it has created all attributes.
            sw.Restart();
            root.Verify();
            sw.Stop();
       //     UnityEngine.Debug.Log( "Verify: " + sw.ElapsedMilliseconds + "ms" );

            // Get some stats.
            sw.Restart();
            numLeafs = 0;
            numCells = 0;
            numIntermediateCells = 0;
            root.Traverse( cell =>
            {
                if (cell.IsLeaf) numLeafs++;
                else numIntermediateCells++;
                numCells++;
            } );
            sw.Stop();
      //      UnityEngine.Debug.Log( "Counting stats: " + sw.ElapsedMilliseconds + "ms" );
#endif

            if (IsLoadedFromCache)
            {
                loadStage = LoadStage.Ready;
            }
            else
            {
                loadStage = LoadStage.CreateGfxDataStage2MT;
            }

            return null;
        }

        internal void RemoveAllPerCellInterestAsync()
        {
            root.RemoveAllPerCellInterestAsync();
        }

        internal void ComputeAndSubscribePerCellInterestAsync(LAZRenderer renderer, Plane[] frustumPlanes, Vector3 camOrigin )
        {
            Matrix4x4 worldTransform;
            renderer.GetWorldTransform( out worldTransform );
            root.ComputeAndSubscribePerCellInterestAsync( renderer, frustumPlanes, camOrigin, ref worldTransform );
        }

        internal void TraceClosestPointAsync( LAZRenderer renderer, TraceLineQuery q )
        {
            // Place ray in lazTree local space, this saves many multiplications for puting the vertices in world space.
            Matrix4x4 worldTransform;
            renderer.GetWorldTransform( out worldTransform );
            Matrix4x4 invWorldTransform = worldTransform.inverse;
            var invOrigin  = invWorldTransform.MultiplyPoint( q.start );
            var invDir     = invWorldTransform.MultiplyVector( q.dir );
            Ray invRay     = new Ray(invOrigin, invDir.normalized);
            q.pointSkipStep = Mathf.Max( 1, q.pointSkipStep );
            root.TraceClosestPoint( invRay, q, renderer );
        }

        internal void CreateGfxDataStage1MT( CachedGfxObjects cml )
        {
            int gfxObjectIdx = 0;
            int leafIdx = 0;
            root.CreateGfxDataStage1MT( loader, cml, ref gfxObjectIdx, ref leafIdx );
            loadStage = LoadStage.PostbuildAsync;
        }

        internal void CreateGfxDataStage2MT()
        {
            root.CreateGfxDataStage2MT( this );
            loadStage = LoadStage.CreateGfxDataStage2Async;
        }

        internal void CreateGfxDataStage2Async()
        {
            root.CreateGfxDataStage2Async( this );
            loadStage = LoadStage.CreateGfxDataStage3MT;
        }

        internal void CreateGfxDataStage3MT()
        {
            root.CreateGfxDataStage3MT();
            loadStage = LoadStage.Ready;
        }

        internal void GetAllGfxDatas(out List<LAZOctreeCellGfxData> gfxdatas, out List<int> cellIndices)
        {
            gfxdatas = new List<LAZOctreeCellGfxData>();
            cellIndices = new List<int>();
            root.GetAllGfxDatas( ref gfxdatas, ref cellIndices );
        }

        internal void AssignLODBasedRenderablesAsync()
        {
    //        if (!reassignMeshes) return;
            root.AssignLODBasedRenderables();
        }

        internal void Traverse( Action<LAZOctreeCell> cb )
        {
            root?.Traverse( cb ); 
        }
    }
}