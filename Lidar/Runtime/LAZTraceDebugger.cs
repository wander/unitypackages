﻿using System.Diagnostics;
using UnityEngine;

namespace LAZNamespace
{
    [ExecuteInEditMode()]
    internal class LAZTraceDebugger : MonoBehaviour
    {
        public float radius = 0.2f;
        public float distance = 100;
        public int pointSkipStep = 100;
        public LAZLoader streamer;

        [Header("Debug")]
#if UNITY_EDITOR
        [ReadOnly] public long lastQueryTimeMs;
#endif

        TraceLineQuery query;
        Vector3 foundPoint;
        bool hit;
        Stopwatch sw = new Stopwatch();

        private void Awake()
        {
            if (streamer == null )
                streamer = FindObjectOfType<LAZLoader>();
        }

        private void Update()
        {
            if (streamer == null)
            {
                if (Random.Range( 0, 30 )==0)
                    UnityEngine.Debug.LogWarning( "No streamer set, tracing will not work." );
                return;
            }
   
            if ( query == null )
            {
                sw.Restart();
                query = streamer.QueueTraceLine( transform.position, transform.forward, pointSkipStep, radius, distance );
            }
            else if ( query != null && query.done )
            {
#if UNITY_EDITOR
                sw.Stop();
                lastQueryTimeMs = sw.ElapsedMilliseconds;
#endif
                hit = query.hit; 
                if ( hit )
                {
                    foundPoint = query.vertex.pos;
                }
                query = null;
            }
        }

        private void OnDrawGizmos() 
        {
            if (!hit) 
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine( transform.position, transform.position + transform.forward*distance );
            }
            else
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine( transform.position, foundPoint );
                Gizmos.DrawSphere( foundPoint, 0.5f );
            }
        }
    }
}