# Quest 2 Animated Controllers

This package holds everything you need to add Animated Quest 2 Controllers to your Unity project.
There is a prefab for each hand, with interaction animations and textures already setup.

**! The package relies on the new InputSystem from Unity.**

## Usage:
- Add the Input Action Asset (OculusControllerAnimation) to the input action manager in the scene.
- Attach the prefabs 'OculusTouchForQuest2_Right' & 'OculusTouchForQuest2_Left' to your hand tracking rig.
- You can set the helper text in the inspector or through code