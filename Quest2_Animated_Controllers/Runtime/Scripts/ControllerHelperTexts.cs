using TMPro;
using UnityEngine;

namespace Wander
{
    //Used to set the helper texts on the quest controller prefabs
    public class ControllerHelperTexts : MonoBehaviour
    {
        [SerializeField] private HelperTexts startTexts = new HelperTexts();

        [Header("TextObjects")]
        [SerializeField] private GameObject button1Parent;
        [SerializeField] private GameObject button2Parent;
        [SerializeField] private GameObject triggerParent;
        [SerializeField] private GameObject gripParent;
        [SerializeField] private GameObject joypadParent;
        [SerializeField] private GameObject bigTextParent;

        [Header("TMP Textfields")]
        [SerializeField] private TextMeshPro button1Text;
        [SerializeField] private TextMeshPro button2Text;
        [SerializeField] private TextMeshPro triggerText;
        [SerializeField] private TextMeshPro gripText;
        [SerializeField] private TextMeshPro joyText;
        [SerializeField] private TextMeshPro bigText;
        [SerializeField] private TextMeshPro bigTextDescription;

        private void Start()
        {
            SetHelperTexts(startTexts);
        }

        public void SetHelperTexts(HelperTexts pTexts)
        {
            button1Text.text = pTexts.Button1;
            button2Text.text = pTexts.Button2;
            triggerText.text = pTexts.Trigger;
            gripText.text = pTexts.Grip;
            joyText.text = pTexts.Joy;
            bigText.text = pTexts.BigTextTitle;
            bigTextDescription.text = pTexts.BigTextDescription;

            //Disable unused text panels
            button1Parent.SetActive(!string.IsNullOrWhiteSpace(pTexts.Button1));
            button2Parent.SetActive(!string.IsNullOrWhiteSpace(pTexts.Button2));
            triggerParent.SetActive(!string.IsNullOrWhiteSpace(pTexts.Trigger));
            gripParent.SetActive(!string.IsNullOrWhiteSpace(pTexts.Grip));
            joypadParent.SetActive(!string.IsNullOrWhiteSpace(pTexts.Joy));
            bigTextParent.SetActive(!string.IsNullOrWhiteSpace(pTexts.BigTextTitle));
        }

        [System.Serializable]
        public struct HelperTexts
        {
            public string Trigger;
            public string Grip;
            public string Button1;
            public string Button2;
            public string Joy;
            public string BigTextTitle;
            public string BigTextDescription;
        }
    }
}
