using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
    //Enables animation for quest controllers
    public class QuestAnimationController : MonoBehaviour
    {
        private Animator controllerAnimator = null;

        [SerializeField] private InputActionProperty primaryButton;
        [SerializeField] private InputActionProperty secondaryButton;
        [SerializeField] private InputActionProperty startButton;

        [SerializeField] private InputActionProperty joystick;

        [SerializeField] private InputActionProperty trigger;
        [SerializeField] private InputActionProperty grip;

        private void Start()
        {
            controllerAnimator = GetComponent<Animator>();

            //X
            primaryButton.action.performed += (x) => controllerAnimator.SetFloat("Button 1", 1);
            primaryButton.action.canceled += (x) => controllerAnimator.SetFloat("Button 1", 0);

            //Y
            secondaryButton.action.performed += (x) => controllerAnimator.SetFloat("Button 2", 1);
            secondaryButton.action.canceled += (x) => controllerAnimator.SetFloat("Button 2", 0);

            //Menu
            startButton.action.performed += (x) => controllerAnimator.SetFloat("Button 3", 1);
            startButton.action.canceled += (x) => controllerAnimator.SetFloat("Button 3", 0);

            //Trigger
            trigger.action.performed += (x) => controllerAnimator.SetFloat("Trigger", 1);
            trigger.action.canceled += (x) => controllerAnimator.SetFloat("Trigger", 0);

            //Grip
            grip.action.performed += (x) => controllerAnimator.SetFloat("Grip", 1);
            grip.action.canceled += (x) => controllerAnimator.SetFloat("Grip", 0);

        }

        private void Update()
        {
            //Joystick
            Vector2 _joystickMovement = joystick.action.ReadValue<Vector2>();

            controllerAnimator.SetFloat("Joy Y", _joystickMovement.y);
            controllerAnimator.SetFloat("Joy X", _joystickMovement.x);
        }
    }
}
