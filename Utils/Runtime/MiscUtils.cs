﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Wander
{
    public static class MiscUtils
    {
        public static T Random<T>( this T[] arr )
        {
            return arr[UnityEngine.Random.Range( 0, arr.Length )];
        }

        public static T Random<T>( this T[] arr, int min, int count )
        {
            return arr[UnityEngine.Random.Range( min, Mathf.Min( count, arr.Length ) )];
        }

        public static T Random<T>( this List<T> list )
        {
            return list[UnityEngine.Random.Range( 0, list.Count )];
        }

        public static T Random<T>( this List<T> list, int min, int count )
        {
            return list[UnityEngine.Random.Range( min, Mathf.Min( count, list.Count ) )];
        }

        public static KeyValuePair<T, R> Random<T, R>( this Dictionary<T, R> dic )
        {
            int r = UnityEngine.Random.Range(0, dic.Count);
            foreach (var kvp in dic)
                if (r-- == 0)
                    return kvp;
            throw new System.InvalidOperationException( "Dictionary does not contain elements" );
        }

        public static KeyValuePair<T, R> Random<T, R>( this Dictionary<T, R> dic, int min, int count )
        {
            int r = UnityEngine.Random.Range(0, count) + min;
            foreach (var kvp in dic)
                if (r-- == 0)
                    return kvp;
            throw new System.InvalidOperationException( "Min/count out of dictionary range" );
        }

        public static string ToAssetsPath( this string fullPath )
        {
            string result = fullPath.Replace( Application.dataPath, "" );
            result = "Assets" + result;
            result = result.Replace( "\\", "/" );
            return result;
        }

        /* Converts , to ., then does the parse. */
        public static float ToFloat( this string s )
        {
            s = s.Replace( ',', '.' );
            try
            {
                return float.Parse( s, System.Globalization.CultureInfo.InvariantCulture );
            }
            catch (Exception e)
            {
                Debug.LogException( e );
                return 0;
            }
        }

        public static double ToDouble( this string s )
        {
            s = s.Replace( ',', '.' );
            try
            {
                return double.Parse( s, System.Globalization.CultureInfo.InvariantCulture );
            }
            catch (Exception e)
            {
                Debug.LogException( e );
                return 0;
            }
        }

        public static int ParseInt( string s )
        {
            if (string.IsNullOrEmpty( s ))
                return 0;

            bool isNegative = s[0] == '-';
            int res = 0;
            int i   = isNegative ? 1 : 0;
            char c  = s[i];
            while (c != '\0')
            {
                if (c < '0' || c > '9')
                    return 0; // weird character (could also switch to int.parse here if desired
                res = (res * 10) + (c - '0');
                if (++i == s.Length)
                    break;
                c = s[i];
            }

            return res * (isNegative ? -1 : 1);
        }

        static double[] _denoms =
        {
            1,
            1/10.0,
            1/100.0,
            1/1000.0,
            1/10000.0,
            1/100000.0,
            1/1000000.0,
            1/10000000.0,
            1/100000000.0,
            1/1000000000.0,
            1/10000000000.0,
            1/100000000000.0,
            1/1000000000000.0,
            1/10000000000000.0,
            1/100000000000000.0,
            1/1000000000000000.0,
            1/10000000000000000.0,
            1/100000000000000000.0,
            1/1000000000000000000.0,
        };

        public static double ParseDouble( string s )
        {
            if (string.IsNullOrEmpty( s ))
                return 0;

            long res = 0;
            bool isNegative = s[0] =='-';
            int i   = isNegative ? 1 : 0;
            char c  = s[i];
            int decimalPos = -1;

            while (c != '\0')
            {
                if (c == '.')
                    decimalPos = i;
                else if (c >= '0' && c <= '9')
                    res = (res * 10) + (c - '0');
                else
                { // weird character inside
                    if (double.TryParse( s, out double dd ))
                        return dd;
                    return 0;
                }
                if (++i == s.Length)
                    break;
                c = s[i];
            }

            double d = res;
            if (decimalPos != -1) d *= _denoms[i-decimalPos-1];
            if (isNegative) d = -d;

            return d;
        }

        public static void Destroy( this UnityEngine.Object o )
        {
            if (o == null) return;
            if (o is Transform transform)
            {
                o = transform.gameObject;
            }
            if (!Application.isPlaying)
                UnityEngine.Object.DestroyImmediate( o );
            else
                UnityEngine.Object.Destroy( o );
        }

        public static void SetLayerRecursively( this GameObject go, int layer )
        {
            if (go == null)
                return;

            go.layer = layer;

            for (int i = 0;i< go.transform.childCount;i++)
            {
                SetLayerRecursively( go.transform.GetChild( i ).gameObject, layer );
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static bool Raycast( this Vector3 pos, Vector3 dir, float dist, LayerMask mask, out RaycastHit hit )
        {
            return Physics.Raycast( pos, dir, out hit, dist, mask );
        }

        // This does not return a zero vector on a 'random threshold set by unity' when it finds the vector too small to do a normalize.
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static Vector3 normalize2( this Vector3 v )
        {
            float l = (v.x*v.x) + (v.y*v.y) + (v.z*v.z);
            // do not check for 'almost zero'
            if (l == 0) return Vector3.zero;
            // just try normalize
            l = 1.0f/l;
            return v*l;
        }

        public static T GetOrAddComponent<T>( this GameObject go ) where T : Component
        {
            if (go == null)
                return null;
            var c = go.GetComponent<T>();
            if (c == null)
                c = go.AddComponent<T>();
            return c;
        }

        public static List<double[]> CsvToDoubles( string path, char delim )
        {
            List<double[]> result = new List<double[]>();
            if (!File.Exists( path ))
                return result;
            string [] rows = File.ReadAllLines( path );
            for (int i = 1;i < rows.Length;i++)
            {
                string [] cells = rows[i].Split( delim );
                double [] vs = new double[cells.Length];
                for (int j = 0;j < cells.Length;j++)
                {
                    vs[j] = cells[j].ToDouble();
                }
                result.Add( vs );
            }
            return result;
        }

        public static Transform FindChildRecursive2( this Transform parent, string name )
        {
            foreach (Transform child in parent)
            {
                if (child.name == name)
                    return child;

                var result = child.FindChildRecursive2(name);
                if (result != null)
                    return result;
            }
            return null;
        }

        public static bool HasContent( this string s )
        {
            return !string.IsNullOrWhiteSpace( s );
        }

        public static void RemoveSwapBack2<T>( this List<T> list, Func<T, bool> predicate )
        {
            for ( int i = 0; i < list.Count; i++)
            {
                if (predicate( list[i] ))
                {
                    list[i] = list[list.Count-1];
                    list.RemoveAt(list.Count-1);
                }
            }
        }

        // https://stackoverflow.com/questions/19497765/equivalent-of-cs-reinterpret-cast-in-c-sharp
        public static unsafe TDest ReinterpretCast<TSource, TDest>( TSource source )
        {
            var sourceRef = __makeref(source);
            var dest = default(TDest);
            var destRef = __makeref(dest);
            *(IntPtr*)&destRef = *(IntPtr*)&sourceRef;
            return __refvalue(destRef, TDest);
        }

        public static Color RandomColor( Color lowRandom, Color highRandom )
        {
            return new Color(
                UnityEngine.Random.Range( lowRandom.r, highRandom.r ),
                UnityEngine.Random.Range( lowRandom.g, highRandom.g ),
                UnityEngine.Random.Range( lowRandom.b, highRandom.b )
                );
        }

        public static Material LitMaterialFromColor(Color c)
        {
            var baseFromShader = Shader.Find( "Universal Render Pipeline/Lit" );
            var m = new Material( baseFromShader );
            m.color = c;
            return m;
        }
    }
}