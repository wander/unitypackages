﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Wander
{
    public static class GeomUtil
    {
        /*  Reduce resolution of a normalized vector. */
        public static Vector3 ReduceResolution(this Vector3 v, float resolution=32)
        {
            Debug.Assert( Mathf.Abs( Vector3.Dot( v, v )-1 )<0.001f ); // Assert normalized.
            float invRes = 1.0f / resolution;
            var n = v * resolution;
            n.x = Mathf.Round( n.x );
            n.y = Mathf.Round( n.y );
            n.z = Mathf.Round( n.z );
            return n*invRes;
        }

        public static Vector3 CrossWithUpVector( Vector3 v )
        {
            return new Vector3( -v.z, 0, v.x );
        }

        public static void GenerateUvs( Vector3 n, Vector3 v0, Vector3 v1, Vector3 v2, out Vector2 uv0, out Vector2 uv1, out Vector2 uv2 )
        {
            Vector3 u = CrossWithUpVector( n );
            Debug.Assert( u.IsSane(), "Vector contains invalid number" );

            if (Vector3.Dot( u, u ) < 0.01f)
                u = Vector3.right;
            else
                u = u.normalized;

            Debug.Assert( u.IsSane(), "Vector contains invalid number" );
            Vector3 v = (Vector3.Cross(n, u)).normalized;
            Debug.Assert( v.IsSane(), "Vector contains invalid number" );

            uv0 = new Vector2( Vector3.Dot( v0, u ), Vector3.Dot( v0, v ) );
            uv1 = new Vector2( Vector3.Dot( v1, u ), Vector3.Dot( v1, v ) );
            uv2 = new Vector2( Vector3.Dot( v2, u ), Vector3.Dot( v2, v ) );

            Debug.Assert( uv0.IsSane() && uv1.IsSane() && uv2.IsSane(), "Vector contains invalid number" );
        }
    }
}