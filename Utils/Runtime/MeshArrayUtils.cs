﻿using System;
using System.Buffers;
using System.Diagnostics;
using System.IO;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Rendering;
using System.Linq;
using System.Threading.Tasks;

namespace Wander
{
    /* Generalized naming for loading mesh array's from formats like .Obj, .Ply, .STL, etc. */
    public struct MeshArrays
    {
        public Vector3 [] vertices;
        public Vector3 [] normals;
        public Color   [] colors;
        public int     [] indices;

        public Mesh ToMesh()
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            Mesh m = new Mesh();
            m.indexFormat = IndexFormat.UInt32;
            m.SetVertices( vertices );
            m.SetNormals( normals );
            m.SetColors( colors );
            if (indices == null || indices.Length == 0)
            {
                // Assume all points
                UnityEngine.Debug.Log( "No Indices found -> Assuming all points" );
                int [] indices = new int[vertices.Length];
                for (int i = 0;i < vertices.Length;i++)
                    indices[i] = i;
                m.SetIndices( indices, MeshTopology.Points, 0 );
            }
            else
            {
                // Assume triangles
                if (indices.Length % 3 == 0)
                    m.SetIndices( indices, MeshTopology.Triangles, 0 );
                else if (indices.Length % 4 == 0)
                    m.SetIndices( indices, MeshTopology.Quads, 0 );
            }
            st.Stop();
            UnityEngine.Debug.Log( nameof( ToMesh ) + " call time: " + st.Elapsed.TotalSeconds + " seconds" );
            return m;
        }
    }


    /* Generalized naming for loading mesh array's from formats like .Obj, .Ply, .STL, etc. */
    public struct MeshNativeArrays
    {
        [System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Sequential )]
        public struct Vertex
        {
            public Vector3 pos;
            public Vector3 nor;
            public Color col;
        }

        public NativeArray<Vertex> vertices;
        public NativeArray<int> indices;
        bool disposed;

        public Mesh ToMeshPositionNormalColor( bool markUploadedMeshNoLongerReadable )
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontResetBoneBounds | MeshUpdateFlags.DontRecalculateBounds;
            Mesh m = new Mesh();
            m.indexFormat = IndexFormat.UInt32;
            var layout = new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Tangent, VertexAttributeFormat.Float32, 4),
            };
            m.SetVertexBufferParams( vertices.Length, layout );
            m.SetVertexBufferData( vertices, 0, 0, vertices.Length, 0, flags );
            m.SetIndexBufferParams( indices.Length, IndexFormat.UInt32 );
            m.SetIndexBufferData( indices, 0, 0, indices.Length, flags );
            var submesh = new []
            {
                new SubMeshDescriptor(0, indices.Length, MeshTopology.Triangles)
            };
            m.SetSubMeshes( submesh, flags );
            m.RecalculateBounds( MeshUpdateFlags.DontValidateIndices|MeshUpdateFlags.DontNotifyMeshUsers|MeshUpdateFlags.DontResetBoneBounds );
            m.UploadMeshData( markUploadedMeshNoLongerReadable );
            st.Stop();
            //     UnityEngine.Debug.Log( nameof( ToMeshPositionNormalColor ) + " call time: " + st.Elapsed.TotalSeconds + " seconds" );
            return m;
        }

        public unsafe static MeshNativeArrays FromCache( string file )
        {
            MeshNativeArrays arr = new MeshNativeArrays();
            arr.disposed = true;
            if (!File.Exists( file ))
                return arr;
            using var fs = new BinaryReader( File.Open( file, FileMode.Open ) );
            int numVertices = fs.ReadInt32();
            int numIndices  = fs.ReadInt32();
            arr.vertices = new NativeArray<Vertex>( numVertices, Allocator.Persistent, NativeArrayOptions.UninitializedMemory );
            arr.indices  = new NativeArray<int>( numIndices, Allocator.Persistent, NativeArrayOptions.UninitializedMemory );
            void* pVertices = NativeArrayUnsafeUtility.GetUnsafePtr( arr.vertices );
            void* pIndices  = NativeArrayUnsafeUtility.GetUnsafePtr( arr.indices );
            arr.disposed = false;
            var spanVertices = new Span<byte>( pVertices, sizeof( Vertex )*numVertices );
            var spanIndices  = new Span<byte>( pIndices, sizeof( int )*numIndices );
            byte [] byteVertices = ArrayPool<byte>.Shared.Rent( sizeof( Vertex )*numVertices );
            byte [] byteIndices  = ArrayPool<byte>.Shared.Rent( sizeof( int )*numIndices );
            try
            {
                fs.Read( byteVertices, 0, sizeof( Vertex )*numVertices );
                fs.Read( byteIndices, 0, sizeof( int )*numIndices );
                byteVertices.CopyTo( pVertices, sizeof( Vertex )*numVertices );
                byteIndices.CopyTo( pIndices, sizeof( int )*numIndices );
            }
            finally
            {
                ArrayPool<byte>.Shared.Return( byteVertices );
                ArrayPool<byte>.Shared.Return( byteIndices );
            }
            return arr;
        }

        public unsafe void ToCache( string file )
        {
            if (disposed || !vertices.IsCreated || !indices.IsCreated || vertices.Length < 1024 || indices.Length < 1024)
                return;
            using (var fs = new BinaryWriter( File.Open( file, FileMode.Create ) ))
            {
                fs.Write( vertices.Length );
                fs.Write( indices.Length );
                void* pVertices = NativeArrayUnsafeUtility.GetUnsafePtr( vertices );
                void* pIndces = NativeArrayUnsafeUtility.GetUnsafePtr( indices );
                fs.Write( new ReadOnlySpan<byte>( pVertices, vertices.Length*sizeof( Vertex ) ) );
                fs.Write( new ReadOnlySpan<byte>( pIndces, indices.Length*sizeof( int ) ) );
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                var thisCpy = this;
                Task.Run( () =>
                {
                    if (thisCpy.vertices.IsCreated) thisCpy.vertices.Dispose();
                    if (thisCpy.indices.IsCreated) thisCpy.indices.Dispose();
                } );


            }
        }
    }

    public struct PointCloudNativeArrays
    {
        [System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Sequential )]
        public struct Vertex
        {
            public Vector3 pos;
            public Color col;
        }

        public NativeArray<Vertex> vertices;
        bool disposed;

        public Vector3 [] ToPositionsAndDispose()
        {
            var array = vertices.ToArray().Select( v => v.pos ).ToArray();
            Dispose();
            return array;
        }

        public Mesh ToMeshPositionColor( bool markUploadedMeshNoLongerReadable )
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontResetBoneBounds | MeshUpdateFlags.DontRecalculateBounds;
            Mesh m = new Mesh();
            var layout = new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.Float32, 4),
            };
            m.SetVertexBufferParams( vertices.Length, layout );
            m.SetVertexBufferData( vertices, 0, 0, vertices.Length, 0, flags );
            int [] indices = new int [vertices.Length];
            for (int i = 0;i < indices.Length;i++) indices[i] = i;
            m.SetIndexBufferParams( indices.Length, IndexFormat.UInt32 );
            m.SetIndexBufferData( indices, 0, 0, indices.Length, flags );
            var submesh = new []
            {
                new SubMeshDescriptor(0, indices.Length, MeshTopology.Points)
            };
            m.SetSubMeshes( submesh, flags );
            m.RecalculateBounds( flags );
            m.UploadMeshData( markUploadedMeshNoLongerReadable );
            st.Stop();
            //     UnityEngine.Debug.Log( nameof( ToMeshPositionNormalColor ) + " call time: " + st.Elapsed.TotalSeconds + " seconds" );
            return m;
        }

        public unsafe static PointCloudNativeArrays FromCache( string file )
        {
            PointCloudNativeArrays arr = new PointCloudNativeArrays();
            arr.disposed = true;
            if (!File.Exists( file ))
                return arr;
            using var fs = new BinaryReader( File.Open( file, FileMode.Open ) );
            int numVertices = fs.ReadInt32();
            int numIndices  = fs.ReadInt32();
            arr.vertices = new NativeArray<Vertex>( numVertices, Allocator.Persistent, NativeArrayOptions.UninitializedMemory );
            void* pVertices = NativeArrayUnsafeUtility.GetUnsafePtr( arr.vertices );
            arr.disposed = false;
            var spanVertices = new Span<byte>( pVertices, sizeof( Vertex )*numVertices );
            byte [] byteVertices = ArrayPool<byte>.Shared.Rent( sizeof( Vertex )*numVertices );
            byte [] byteIndices  = ArrayPool<byte>.Shared.Rent( sizeof( int )*numIndices );
            try
            {
                fs.Read( byteVertices, 0, sizeof( Vertex )*numVertices );
                fs.Read( byteIndices, 0, sizeof( int )*numIndices );
                byteVertices.CopyTo( pVertices, sizeof( Vertex )*numVertices );
            }
            finally
            {
                ArrayPool<byte>.Shared.Return( byteVertices );
                ArrayPool<byte>.Shared.Return( byteIndices );
            }
            return arr;
        }

        public unsafe void ToCache( string file )
        {
            if (disposed || !vertices.IsCreated || vertices.Length < 1024 )
                return;
            Directory.CreateDirectory( Path.GetDirectoryName( file ) );
            using (var fs = new BinaryWriter( File.Open( file, FileMode.CreateNew ) ))
            {
                fs.Write( vertices.Length );
                void* pVertices = NativeArrayUnsafeUtility.GetUnsafePtr( vertices );
                fs.Write( new ReadOnlySpan<byte>( pVertices, vertices.Length*sizeof( Vertex ) ) );
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                var thisCpy = this;
                Task.Run( () =>
                {
                    if (thisCpy.vertices.IsCreated) thisCpy.vertices.Dispose();
                } );
            }
        }
    }
}